package webservice;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.example.tcc.R;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

public class EnviaTreinoDiaDaSemana extends AsyncTask<JSONArray, Void, String> 
{
	Fragment F;
	JSONArray jj;
	public EnviaTreinoDiaDaSemana(Fragment f, JSONArray jA)
	{
		F = f;
		jj = jA;
	}
	@Override
	protected String doInBackground(JSONArray... json) 
	{
		String texto = "";
		HttpClient cliente = new DefaultHttpClient();
		
		String servidor = F.getString(R.string.servidorAtual);
		HttpPost post = new HttpPost("http://" + servidor + "/TCC/ConfiguraNovoTreino.php");
		
		List<NameValuePair> lista = new ArrayList<NameValuePair>();
		lista.add(new BasicNameValuePair("dadosTreino", jj.toString()));
		Log.d("MinhaTag", "Json enviado: " + jj.toString());
		
		try {
			post.setEntity(new UrlEncodedFormEntity(lista));
			ResponseHandler<String> cuida = new BasicResponseHandler();
			texto = cliente.execute(post, cuida);
		} catch (UnsupportedEncodingException e) {
			Log.e("MinhaTag", "Erro na configuracao do metodo post");
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//JSONObject resJSON = new JSONObject(texto);		
		Log.d("MinhaTag", "Valor de resposta: " + texto);
		return null;
	}
	
	@Override
	protected void onPostExecute(String resultado )
	{
		
	}

}
