package webservice;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import modelos.Usuario;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import telas.TelaPerfil2;

import com.example.tcc.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

public class EnviaDietaDiaDaSemana extends AsyncTask<JSONArray, Void, String> 
{
	Fragment F;
	JSONObject jj;
	Usuario usuario;
	
	public EnviaDietaDiaDaSemana(Fragment f, JSONObject jA, Usuario u)
	{
		F = f;
		jj = jA;
		usuario = u;
	}
	@Override
	protected String doInBackground(JSONArray... json) 
	{
		String texto = "";
		HttpClient cliente = new DefaultHttpClient();
		
		String servidor = F.getString(R.string.servidorAtual);
		HttpPost post = new HttpPost("http://" + servidor + "/TCC/ConfiguraNovaDieta.php");
		
		List<NameValuePair> lista = new ArrayList<NameValuePair>();
		lista.add(new BasicNameValuePair("dadosDieta", jj.toString()));
		Log.d("MinhaTag", "Json enviado: " + jj.toString());
		
		try {
			post.setEntity(new UrlEncodedFormEntity(lista));
			ResponseHandler<String> cuida = new BasicResponseHandler();
			texto = cliente.execute(post, cuida);
		} catch (UnsupportedEncodingException e) {
			Log.e("MinhaTag", "Erro na configuracao do metodo post");
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//JSONObject resJSON = new JSONObject(texto);		
		Log.d("MinhaTag", "Valor de resposta: " + texto);
		return texto;
	}
	
	@Override
	protected void onPostExecute(String resultado )
	{
		try 
		{
			JSONObject resposta = new JSONObject(resultado);
			
			if(resposta.getInt("status") == 1)
			{
				SharedPreferences c = F.getActivity().getSharedPreferences("Usuario", F.getActivity().MODE_PRIVATE);
				Editor edit = c.edit();
				
				edit.putString("usuario", usuario.toJSON().toString());
				
				edit.commit();
				
				Toast.makeText(F.getActivity(), "Nova Dieta Salva com sucesso", Toast.LENGTH_LONG).show();
				//F.startActivity(new Intent(F.getActivity(), TelaPerfil2.class));
			}
			else
			{
				Toast.makeText(F.getActivity(), "Nao foi possivel salvar a nova dieta, tente novamente", Toast.LENGTH_LONG).show();
			}
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
	}

}
