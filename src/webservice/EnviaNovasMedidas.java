package webservice;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import modelos.Usuario;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import telas.TelaNovasMedidas;

import com.example.tcc.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class EnviaNovasMedidas extends AsyncTask<Void, Void, String> {
	
	Usuario usuario;
	Activity TelaMae;
	String respostaServidor;
	
	public EnviaNovasMedidas(Usuario u, Activity a)
	{
		usuario = u;
		TelaMae = a;
	}
	
	@Override
	protected String doInBackground(Void... params) {
		
		String retorno = "Falha";
		
		JSONObject resultado = new JSONObject();
		
		HttpClient cliente = new DefaultHttpClient();
		
		JSONObject str = new JSONObject();
		List<NameValuePair> lista = new ArrayList<NameValuePair>();
		
		try
		{			
			try
			{
				str.put("id", usuario.getID());
				
				if(usuario.existePeso())
				{
					str.put("PesoAtual", usuario.getPesoAtual());
					str.put("PesoObjetivo", usuario.getPesoObjetivo());
					str.put("bPeso", 1);
				}
				else
				{
					str.put("PesoAtual", -1);
					str.put("PesoObjetivo", -1);
					str.put("bPeso", 0);
				}
				
				if(usuario.existeTorax())
				{
					str.put("ToraxAtual", usuario.getToraxAtual());
					str.put("ToraxObjetivo", usuario.getToraxObjetivo());
					str.put("bTorax", 1);
				}
				else
				{
					str.put("ToraxAtual", -1);
					str.put("ToraxObjetivo", -1);
					str.put("bTorax", 0);
				}
				
				if(usuario.existeBraco())
				{
					str.put("BracoAtual", usuario.getBracoAtual());
					str.put("BracoObjetivo", usuario.getBracoObjetivo());
					str.put("bBraco", 1);
				}
				else
				{
					str.put("BracoAtual", -1);
					str.put("BracoObjetivo", -1);
					str.put("bBraco", 0);
				}
				
				if(usuario.existeCintura())
				{
					str.put("CinturaAtual", usuario.getCinturaAtual());
					str.put("CinturaObjetivo", usuario.getCinturaObjetivo());
					str.put("bCintura", 1);
				}
				else
				{
					str.put("CinturaAtual", -1);
					str.put("CinturaObjetivo", -1);
					str.put("bCintura", 0);
				}
				
				if(usuario.existeCostas())
				{
					str.put("CostasAtual", usuario.getCostasAtual());
					str.put("CostasObjetivo", usuario.getCostasObjetivo());
					str.put("bCostas", 1);
				}
				else
				{
					str.put("CostasAtual", -1);
					str.put("CostasObjetivo", -1);
					str.put("bCostas", 0);
				}
				
				if(usuario.existeCoxas())
				{
					str.put("CoxasAtual", usuario.getCoxasAtual());
					str.put("CoxasObjetivo", usuario.getCoxasObjetivo());
					str.put("bCoxas", 1);
				}
				else
				{
					str.put("CoxasAtual", -1);
					str.put("CoxasObjetivo", -1);
					str.put("bCoxas", 0);
				}
				
				if(usuario.existePanturrilha())
				{
					str.put("PanturrilhaAtual", usuario.getPanturrilhaAtual());
					str.put("PanturrilhaObjetivo", usuario.getPanturrilhaObjetivo());
					str.put("bPanturrilha", 1);
				}
				else
				{
					str.put("PanturrilhaAtual", -1);
					str.put("PanturrilhaObjetivo", -1);
					str.put("bPanturrilha", 0);
				}
				
				
				lista.add(new BasicNameValuePair("dadosNovasMedidas", str.toString()));
			}
			catch(JSONException e)
			{
				e.printStackTrace();
			}
			
			String servidor = TelaMae.getString(R.string.servidorAtual);
			HttpPost post = new HttpPost("http://" + servidor + "/TCC/MudaMedidasUsuarios.php");
			
			try 
			{
				post.setEntity(new UrlEncodedFormEntity(lista));
			} 
			catch (UnsupportedEncodingException e) 
			{
				e.printStackTrace();
			}
			
			ResponseHandler<String> cuida = new BasicResponseHandler();
			respostaServidor = cliente.execute(post, cuida);
			
			resultado = new JSONObject(respostaServidor);
			
			int status = -1;
			try 
			{
				status = resultado.getInt("status");
			} 
			catch (JSONException e) 
			{
				e.printStackTrace();
			}
			
			if(status == 1)
			{
				//{"status":1,"email":"teste@teste.com","senha":"12345","nome":"Usuario","sobrenome":"Teste","pesoAtual":"75"}
				try
				{
					usuario.setPeso((float)resultado.getDouble("pesoAtual"), (float)resultado.getDouble("pesoObjetivo"), resultado.getBoolean("peso"));
					usuario.setTorax((float)resultado.getDouble("toraxAtual"), (float)resultado.getDouble("toraxObjetivo"), resultado.getBoolean("torax"));
					usuario.setBraco((float)resultado.getDouble("bracoAtual"), (float)resultado.getDouble("bracoObjetivo"), resultado.getBoolean("braco"));
					usuario.setCintura((float)resultado.getDouble("cinturaAtual"), (float)resultado.getDouble("cinturaObjetivo"), resultado.getBoolean("cintura"));
					usuario.setCostas((float)resultado.getDouble("costasAtual"), (float)resultado.getDouble("costasObjetivo"), resultado.getBoolean("costas"));
					usuario.setCoxas((float)resultado.getDouble("coxasAtual"), (float)resultado.getDouble("coxasObjetivo"), resultado.getBoolean("coxas"));
					usuario.setPanturrilha((float)resultado.getDouble("panturrilhaAtual"), (float)resultado.getDouble("panturrilhaObjetivo"), resultado.getBoolean("panturrilha"));
					
					SharedPreferences c;
					
					c = TelaMae.getSharedPreferences("Usuario", TelaMae.MODE_PRIVATE);
					
					Editor edit = c.edit();
					edit.putString("usuario", usuario.toJSON().toString());
					
					edit.commit();
					retorno = "Sucesso";
					
				}
				catch(JSONException e)
				{
					e.printStackTrace();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Deu erro na AsysncTask de enviar novas medidas, provavelmente JSON");
		}
		
		return retorno;
	}
	
	@Override
	protected void onPostExecute(String resultado )
	{
		if(resultado == "Sucesso")
		{
			TelaMae.startActivity(new Intent(TelaMae.getApplicationContext(), TelaNovasMedidas.class));
		}
		else
		{
			Toast.makeText(TelaMae.getApplicationContext(), "Erro ao importar as novas medidas", Toast.LENGTH_LONG).show();
		}
	}

}
