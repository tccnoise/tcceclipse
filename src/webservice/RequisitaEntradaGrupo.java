package webservice;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import modelos.Usuario;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import telas.TelaGrupo;
import telas.TelaNovasMedidas;
import telas.TelaPerfil2;

import com.example.tcc.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class RequisitaEntradaGrupo extends AsyncTask<Void, Void, String> {
	
	Activity TelaMae;
	String respostaServidor;
	Usuario usuario;
	int iGrupo;
	
	public RequisitaEntradaGrupo(Activity a, Usuario u, int idGrupo)
	{
		TelaMae = a;
		usuario = u;
		iGrupo = idGrupo;
	}
	
	@Override
	protected String doInBackground(Void... params) {
		
		String retorno = "Voc� ja pertence a esse grupo";
		boolean jaPertenceAoGrupo = false;
		
		HttpClient cliente = new DefaultHttpClient();
		
		JSONObject str = new JSONObject();
		List<NameValuePair> lista = new ArrayList<NameValuePair>();
		
		for(int i = 0; i < usuario.getGrupos().size(); i++)
		{
			if(iGrupo == usuario.getGrupos().get(i).getId())
			{
				jaPertenceAoGrupo = true;
				break;
			}
		}
		
		if(!jaPertenceAoGrupo)
		{
			try
			{			
				try
				{
					str.put("idUsuario", usuario.getID());
					str.put("idGrupo", iGrupo);
					
					lista.add(new BasicNameValuePair("dadosRequisicao", str.toString()));
				}
				catch(JSONException e)
				{
					e.printStackTrace();
				}
				
				String servidor = TelaMae.getString(R.string.servidorAtual);
				HttpPost post = new HttpPost("http://" + servidor + "/TCC/RequisitarEntradaGrupo.php");
				
				try 
				{
					post.setEntity(new UrlEncodedFormEntity(lista));
				} 
				catch (UnsupportedEncodingException e) 
				{
					e.printStackTrace();
				}
				
				ResponseHandler<String> cuida = new BasicResponseHandler();
				respostaServidor = cliente.execute(post, cuida);
				
				
				retorno = respostaServidor;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Log.e("MinhaTag", "Deu erro na AsysncTask de enviar Requisicao");
			}
		}
		
		return retorno;
	}
	
	@Override
	protected void onPostExecute(String resultado)
	{
		if(resultado.contains("Sucesso"))
		{
			Log.d("MinhaTag", "caiu no if");
			Toast.makeText(TelaMae, "Requisi��o enviada com sucesso", Toast.LENGTH_LONG).show();
			TelaMae.startActivity(new Intent(TelaMae, TelaGrupo.class));
		}
		else
		{
			Log.d("MinhaTag", "caiu no else");
			Toast.makeText(TelaMae, resultado, Toast.LENGTH_LONG).show();
		}
	}

}
