package webservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.tcc.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;

public class CarregaInformacao extends AsyncTask<Void, Void, JSONObject> {
	
	private Activity TelaMae;
	String resposta;
	
	public CarregaInformacao(Activity a)
	{
		TelaMae = a;
	}
	
	@Override
	protected JSONObject doInBackground(Void... params) 
	{
		JSONObject resultado = new JSONObject();
		HttpClient cliente = new DefaultHttpClient();
		
		String servidor = TelaMae.getString(R.string.servidorAtual);
		HttpGet get = new HttpGet("http://" + servidor + "/TCC/CarregaInformacao.php");
		ResponseHandler r = new BasicResponseHandler();
		
		try {
			resposta = cliente.execute(get, r);
			try {
				resultado = new JSONObject(resposta);
			} catch (JSONException e) {
				Log.e("MinhaTag", "Erro no Json");
				e.printStackTrace();
			}
		} catch (ClientProtocolException e) {
			Log.e("MinhaTag", "Erro nos http");
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("MinhaTag", "Erro no IO");
			e.printStackTrace();
		}
		
		return resultado;
	}
	
	@Override
	protected void onPostExecute(JSONObject res )
	{
		ArrayList<String> vetorDeString = new ArrayList<String>();
		int i = 0;
		SharedPreferences c;
		
		try {
			while(i < res.getJSONArray("exercicios").length())
			{
				try {
					vetorDeString.add(res.getJSONArray("exercicios").getString(i));
					
				} catch (JSONException e) {
					Log.e("MinhaTag", "Erro no JSON retorno");
					e.printStackTrace();
				}
				i++;
			}
			
			c = TelaMae.getSharedPreferences("Alimentos", TelaMae.MODE_PRIVATE);
			
			Editor edit = c.edit();
			edit.putString("listaAlimentos", res.getJSONArray("alimentos").toString());
			edit.commit();
			
			c = TelaMae.getSharedPreferences("Musculos", TelaMae.MODE_PRIVATE);
			
			Editor edit2 = c.edit();
			edit2.putString("listaMusculos", res.getJSONArray("musculos").toString());
			edit2.commit();
			
		} catch (JSONException e) {
			Log.e("MinhaTag", "Erro no JSON retorno no while");
			e.printStackTrace();
		}
		
		Set<String> stringsExercicios = new HashSet<String>();
		stringsExercicios.addAll(vetorDeString);
		
		//SharedPreferences c;
		
		c = TelaMae.getSharedPreferences("Exercicios", TelaMae.MODE_PRIVATE);
		
		Editor edit = c.edit();
		edit.putStringSet("listaExercicios", stringsExercicios);
		edit.commit();
	}

}
