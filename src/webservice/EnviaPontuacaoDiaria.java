package webservice;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import modelos.Usuario;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import telas.TelaNovasMedidas;
import telas.TelaPerfil2;

import com.example.tcc.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class EnviaPontuacaoDiaria extends AsyncTask<Void, Void, String> {
	
	Activity TelaMae;
	String respostaServidor;
	Usuario usuario;
	
	public EnviaPontuacaoDiaria(Activity a, Usuario u)
	{
		TelaMae = a;
		usuario = u;
	}
	
	@Override
	protected String doInBackground(Void... params) {
		
		String retorno = "Falha";
		
		JSONObject resultado = new JSONObject();
		
		HttpClient cliente = new DefaultHttpClient();
		
		JSONObject str = new JSONObject();
		List<NameValuePair> lista = new ArrayList<NameValuePair>();
		
		try
		{			
			try
			{
				str.put("id", usuario.getID());
				str.put("pontuacao", usuario.getPontos());
				str.put("bonus", usuario.getBonus());
				
				lista.add(new BasicNameValuePair("pontuacaoDiaria", str.toString()));
			}
			catch(JSONException e)
			{
				e.printStackTrace();
			}
			
			String servidor = TelaMae.getString(R.string.servidorAtual);
			HttpPost post = new HttpPost("http://" + servidor + "/TCC/RecebePontuacao.php");
			
			try 
			{
				post.setEntity(new UrlEncodedFormEntity(lista));
			} 
			catch (UnsupportedEncodingException e) 
			{
				e.printStackTrace();
			}
			
			ResponseHandler<String> cuida = new BasicResponseHandler();
			respostaServidor = cliente.execute(post, cuida);
			
			
			resultado = new JSONObject(respostaServidor);
			
			int status = -1;
			try 
			{
				status = resultado.getInt("status");
			} 
			catch (JSONException e) 
			{
				e.printStackTrace();
			}
			
			if(status == 1)
			{
				//{"status":1,"email":"teste@teste.com","senha":"12345","nome":"Usuario","sobrenome":"Teste","pesoAtual":"75"}
				retorno = "Sucesso";
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Deu erro na AsysncTask de enviar Pontuacao Diaria");
		}
		
		return retorno;
	}
	
	@Override
	protected void onPostExecute(String resultado )
	{
		if(resultado == "Sucesso")
		{
			SharedPreferences c;
			
			c = TelaMae.getSharedPreferences("Usuario", TelaMae.MODE_PRIVATE);
			
			Editor edit = c.edit();
			edit.putString("usuario", usuario.toJSON().toString());
			
			edit.commit();
			
			Toast.makeText(TelaMae, "Sucesso ao salvar pontuacao no servidor", Toast.LENGTH_LONG).show();
			TelaMae.startActivity(new Intent(TelaMae, TelaPerfil2.class));
		}
		else
		{
			Toast.makeText(TelaMae, "Erro ao salvar a pontuacao no servidor", Toast.LENGTH_LONG).show();
		}
	}

}
