package webservice;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import modelos.Dieta;
import modelos.Grupo;
import modelos.Usuario;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import telas.TelaGrupo;
import telas.TelaNovoProjeto;
import telas.TelaPerfil;
import telas.TelaPerfil2;
import util.UtilView;

import com.example.tcc.R;

import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class CriaNovoGrupo extends AsyncTask<Void, Void , JSONObject>
{
	Activity TelaMae;
	String texto;
	String nomeG;
	String descG;
	Usuario usuario;
	
	public CriaNovoGrupo(Activity a, String nome, String descricao)
	{
		TelaMae = a;
		nomeG = nome;
		descG = descricao;
	}

	@Override
	protected JSONObject doInBackground(Void... params) 
	{
		String retorno = "Falha";
		JSONObject resposta = null;
		
		JSONObject dados = new JSONObject();
		HttpClient cliente = new DefaultHttpClient();
		List<NameValuePair> lista = new ArrayList<NameValuePair>();
		
		usuario = new Usuario();
		
		SharedPreferences info = TelaMae.getSharedPreferences("Usuario", TelaMae.MODE_PRIVATE);
		
		try 
		{
			usuario.recuperaUsuarioJSON(new JSONObject(info.getString("usuario", "Nao conseguiu recuperar a string de json do usuario no WS de criacao de grupo")));
		} 
		catch (JSONException e1) 
		{
			e1.printStackTrace();
		}
		
		
		try
		{
			dados.put("idAdm", usuario.getID());
			dados.put("nomeGrupo", nomeG);
			dados.put("descGrupo", descG);
			
			lista.add(new BasicNameValuePair("dadosNovoGrupo", dados.toString()));
			
			String servidor = TelaMae.getString(R.string.servidorAtual);
			HttpPost post = new HttpPost("http://" + servidor + "/TCC/CriarGrupo.php");
			
			try 
			{
				post.setEntity(new UrlEncodedFormEntity(lista));
			} 
			catch (UnsupportedEncodingException e) 
			{
				e.printStackTrace();
			}
			
			ResponseHandler<String> cuida = new BasicResponseHandler();
			texto = cliente.execute(post, cuida);
			
			resposta = new JSONObject(texto);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Deu erro na Async Task de Valida Usuario");
		}
		
		return resposta;
	}
	
	@Override
	protected void onPostExecute(JSONObject resultado )
	{
		int status = -1;
		int id = -1;
		
		try 
		{
			status = resultado.getInt("status");
			id = resultado.getInt("idGrupo");
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		if(resultado != null && status == 1)
		{
			Grupo g = new Grupo(usuario);
			
			g.setNomeDoGrupo(nomeG);
			g.setAdmDoGrupo(usuario.getNome());
			g.setDescDoGrupo(descG);
			g.setId(id);
			
			usuario.getGrupos().add(g);
			
			SharedPreferences c;
			
			c = TelaMae.getSharedPreferences("Usuario", TelaMae.MODE_PRIVATE);
			
			Editor edit = c.edit();
			edit.putString("usuario", usuario.toJSON().toString());
			
			edit.commit();
			
			Toast.makeText(TelaMae.getApplicationContext(), "Grupo criado com sucesso", Toast.LENGTH_LONG).show();			
			TelaMae.startActivity(new Intent(TelaMae.getApplicationContext(), TelaGrupo.class));
		}
		else
		{
			Toast.makeText(TelaMae.getApplicationContext(), "N�o foi possivel criar um novo grupo", Toast.LENGTH_LONG).show();
		}
	}
}
