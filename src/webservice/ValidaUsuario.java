package webservice;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import modelos.Dieta;
import modelos.Grupo;
import modelos.Usuario;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import telas.TelaNovoProjeto;
import telas.TelaPerfil;
import telas.TelaPerfil2;
import util.UtilView;

import com.example.tcc.R;

import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class ValidaUsuario extends AsyncTask<Void, Void , String>
{
	Activity TelaMae;
	String texto;
	JSONObject json;
	String req;
	
	public ValidaUsuario(Activity a, JSONObject j, String requisicao)
	{
		TelaMae = a;
		json = j;
		req = requisicao;
	}

	@Override
	protected String doInBackground(Void... params) 
	{
		String retorno = "Falha";
		JSONObject resultado = new JSONObject();
		HttpClient cliente = new DefaultHttpClient();
		List<NameValuePair> lista = new ArrayList<NameValuePair>();
		
		try
		{		
			lista.add(new BasicNameValuePair(req, json.toString()));
			
			String servidor = TelaMae.getString(R.string.servidorAtual);
			HttpPost post = new HttpPost("http://" + servidor + "/TCC/ValidaUsuario.php");
			
			try 
			{
				post.setEntity(new UrlEncodedFormEntity(lista));
			} 
			catch (UnsupportedEncodingException e) 
			{
				e.printStackTrace();
			}
			
			ResponseHandler<String> cuida = new BasicResponseHandler();
			texto = cliente.execute(post, cuida);
			
			Log.d("MinhaTag", texto);
			resultado = new JSONObject(texto);
			
			int status = -1;
			try 
			{
				status = resultado.getInt("status");
			} 
			catch (JSONException e) 
			{
				e.printStackTrace();
			}
			
			if(status == 1)
			{
				//{"status":1,"email":"teste@teste.com","senha":"12345","nome":"Usuario","sobrenome":"Teste","pesoAtual":"75"}
				try
				{
					int id = resultado.getInt("id");
					String email = resultado.getString("email");
					String senha = resultado.getString("senha");
					String nome = resultado.getString("nome");
					String sobrenome = resultado.getString("sobrenome");
					String pathFoto = resultado.getString("fotoPerfil");
					
					Usuario usuario = new Usuario(id, nome, sobrenome, pathFoto, email);
					
					usuario.setPontos((float)resultado.getDouble("pontos"));
					usuario.setBonus((float)resultado.getDouble("bonus"));
					
					if(resultado.getString("dia").equals(null))
					{
						usuario.setDia(resultado.getString("dia"));
					}
					
					if(resultado.getString("mes").equals(null))
					{
						usuario.setMes(resultado.getString("mes"));
					}
					
					if(resultado.getString("ano").equals(null))
					{
						usuario.setAno(resultado.getString("ano"));
					}
					
					usuario.setPeso((float)resultado.getDouble("pesoAtual"), (float)resultado.getDouble("pesoObjetivo"), resultado.getBoolean("peso"));
					usuario.setTorax((float)resultado.getDouble("toraxAtual"), (float)resultado.getDouble("toraxObjetivo"), resultado.getBoolean("torax"));
					usuario.setBraco((float)resultado.getDouble("bracoAtual"), (float)resultado.getDouble("bracoObjetivo"), resultado.getBoolean("braco"));
					usuario.setCintura((float)resultado.getDouble("cinturaAtual"), (float)resultado.getDouble("cinturaObjetivo"), resultado.getBoolean("cintura"));
					usuario.setCostas((float)resultado.getDouble("costasAtual"), (float)resultado.getDouble("costasObjetivo"), resultado.getBoolean("costas"));
					usuario.setCoxas((float)resultado.getDouble("coxasAtual"), (float)resultado.getDouble("coxasObjetivo"), resultado.getBoolean("coxas"));
					usuario.setPanturrilha((float)resultado.getDouble("panturrilhaAtual"), (float)resultado.getDouble("panturrilhaObjetivo"), resultado.getBoolean("panturrilha"));
					
					JSONObject treinoUsuario = pesquisaTreinoUsuario(id);
					
					UtilView util = new UtilView();
					for(int i = 0; i < 7; i++)
					{
						JSONArray elementos = treinoUsuario.getJSONArray(util.stringDiaDaSemana(i));
						
						for(int in = 0; in < elementos.length(); in++)
						{
							String nomeExercicio = elementos.getJSONObject(in).getString("nome");
							int tipo2 = elementos.getJSONObject(in).getInt("tipo2");
							
							String tipoExercicio = "";
							
							if(tipo2 == 0 )
							{
								tipoExercicio = "Dinamico";
							}
							else
							{
								tipoExercicio = "Isometria";
							}
							
							int series = elementos.getJSONObject(in).getInt("series");
							int repeticoes = elementos.getJSONObject(in).getInt("repeticoes");
							int descanso = elementos.getJSONObject(in).getInt("tempoDescanco");
							
							usuario.getTreino(i).AdicionaItemExercicioNaLista(nomeExercicio, tipoExercicio, series, repeticoes, descanso);
						}
					}
					
					JSONObject dietaUsuario = pesquisaDietaUsuario(id);
					
					for(int i = 0; i < 7; i++)
					{
						Dieta dieta = new Dieta();
						JSONArray elementosDieta = dietaUsuario.getJSONObject(util.stringDiaDaSemana(i)).getJSONArray("refeicoes");
						Log.i("MinhaTag", "JSONArray -- " + elementosDieta.toString());
						
						dieta.recuperaDietaJSON(elementosDieta);
						usuario.setDieta(dieta, i);						
					}
					
					JSONArray gruposUsuario = pesquisaGruposUsuario(id);
					
					ArrayList<Grupo> grupos = new ArrayList<Grupo>();
					
					for(int i = 0; i < gruposUsuario.length(); i++)
					{
						Grupo g = new Grupo();
						
						g.recuperaGrupoJSON(gruposUsuario.getJSONObject(i));
						
						grupos.add(g);
					}
					
					usuario.setGrupos(grupos);
					
					String strUsuario = usuario.toJSON().toString();
					
					SharedPreferences c;
					
					c = TelaMae.getSharedPreferences("Usuario", TelaMae.MODE_PRIVATE);
					
					Editor edit = c.edit();
					edit.putString("usuario", strUsuario);
					
					edit.commit();
					retorno = "Sucesso";
					
				}
				catch(JSONException e)
				{
					e.printStackTrace();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Deu erro na Async Task de Valida Usuario");
		}
		
		return retorno;
	}
	
	@Override
	protected void onPostExecute(String resultado )
	{
		if(resultado == "Sucesso")
		{
			TelaMae.startActivity(new Intent(TelaMae.getApplicationContext(), TelaPerfil2.class));
		}
		else
		{
			Toast.makeText(TelaMae.getApplicationContext(), "Mensagem de Erro", Toast.LENGTH_LONG).show();
		}
	}
	
	JSONObject pesquisaTreinoUsuario(int id)
	{
		String resposta;
		
		HttpClient cliente = new DefaultHttpClient();
		JSONObject resultado = new JSONObject();
		List<NameValuePair> lista = new ArrayList<NameValuePair>();
		
		try
		{			
			try
			{
				resultado.put("id", id);
				
				
				
				lista.add(new BasicNameValuePair("TreinoUsuario", resultado.toString()));
			}
			catch(JSONException e)
			{
				e.printStackTrace();
			}
			
			String servidor = TelaMae.getString(R.string.servidorAtual);
			HttpPost post = new HttpPost("http://" + servidor + "/TCC/RecuperaTreino.php");
			
			try 
			{
				post.setEntity(new UrlEncodedFormEntity(lista));
			} 
			catch (UnsupportedEncodingException e) 
			{
				e.printStackTrace();
			}
			
			ResponseHandler<String> cuida = new BasicResponseHandler();
			resposta = cliente.execute(post, cuida);
			try {
				Log.d("MinhaTag", resposta);
				resultado = new JSONObject(resposta);
				//Log.d("MinhaTag", resultado.toString());
			} catch (JSONException e) {
				Log.e("MinhaTag", "Erro ao recuperar o json do servidor com os dados do treino do usuario no metodo da valida usuario");
				e.printStackTrace();
			}
		} catch (ClientProtocolException e) {
			Log.e("MinhaTag", "Erro nos http no metodo da valida usuario");
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("MinhaTag", "Erro no IO no metodo da valida usuario");
			e.printStackTrace();
		}
		
		return resultado;
	}
	
	JSONObject pesquisaDietaUsuario(int id)
	{
		String resposta;
		
		HttpClient cliente = new DefaultHttpClient();
		JSONObject resultado = new JSONObject();
		List<NameValuePair> lista = new ArrayList<NameValuePair>();
		
		try
		{			
			try
			{
				resultado.put("id", id);
				
				
				
				lista.add(new BasicNameValuePair("DietaUsuario", resultado.toString()));
			}
			catch(JSONException e)
			{
				e.printStackTrace();
			}
			
			String servidor = TelaMae.getString(R.string.servidorAtual);
			HttpPost post = new HttpPost("http://" + servidor + "/TCC/RecuperaDieta.php");
			
			try 
			{
				post.setEntity(new UrlEncodedFormEntity(lista));
			} 
			catch (UnsupportedEncodingException e) 
			{
				e.printStackTrace();
			}
			
			ResponseHandler<String> cuida = new BasicResponseHandler();
			resposta = cliente.execute(post, cuida);
			try {
				Log.d("MinhaTag", resposta);
				resultado = new JSONObject(resposta);
				//Log.d("MinhaTag", resultado.toString());
			} catch (JSONException e) {
				Log.e("MinhaTag", "Erro ao recuperar o json do servidor com os dados da dieta do usuario no metodo da valida usuario");
				e.printStackTrace();
			}
		} catch (ClientProtocolException e) {
			Log.e("MinhaTag", "Erro nos http no metodo da valida usuario ao recuperar dieta");
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("MinhaTag", "Erro no IO no metodo da valida usuario ao recuperar dieta");
			e.printStackTrace();
		}
		
		return resultado;
	}
	
	JSONArray pesquisaGruposUsuario(int id)
	{
		String resposta = "";
		JSONArray resultado = new JSONArray();
		HttpClient cliente = new DefaultHttpClient();
		
		String servidor = TelaMae.getString(R.string.servidorAtual);
		HttpGet get = new HttpGet("http://" + servidor + "/TCC/RecuperaGrupos.php?GruposUsuario=" + id);
		ResponseHandler r = new BasicResponseHandler();
		
		try {
			resposta = cliente.execute(get, r);
			try {
				resultado = new JSONArray(resposta);
			} catch (JSONException e) {
				Log.e("MinhaTag", "Erro no Json");
				e.printStackTrace();
			}
		} catch (ClientProtocolException e) {
			Log.e("MinhaTag", "Erro nos http");
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("MinhaTag", "Erro no IO");
			e.printStackTrace();
		}
		
		return resultado;
	}
}
