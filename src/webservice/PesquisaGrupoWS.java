package webservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import modelos.Grupo;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import telas.PesquisaExerciciosFiltros;
import telas.PesquisaGrupos;

import com.example.tcc.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;

public class PesquisaGrupoWS extends AsyncTask<Void, Void, JSONArray> {
	
	private PesquisaGrupos TelaMae;
	String resposta;
	String filtro;
	
	public PesquisaGrupoWS(PesquisaGrupos a, String f)
	{
		TelaMae = a;
		filtro = f;
	}
	
	@Override
	protected JSONArray doInBackground(Void... params) 
	{
		JSONArray resultado = null;
		HttpClient cliente = new DefaultHttpClient();
		
		String servidor = TelaMae.getString(R.string.servidorAtual);
		filtro = filtro.replaceAll(" ", "%20");
		HttpGet get = new HttpGet("http://" + servidor + "/TCC/PesquisaGrupoNome.php?nomeGrupo=" + filtro);
		ResponseHandler r = new BasicResponseHandler();
		
		try {
			resposta = cliente.execute(get, r);
			try {
				Log.d("MinhaTag", resposta);
				resultado = new JSONArray(resposta);
			} catch (JSONException e) {
				Log.e("MinhaTag", "Erro no Json");
				e.printStackTrace();
			}
		} catch (ClientProtocolException e) {
			Log.e("MinhaTag", "Erro nos http");
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("MinhaTag", "Erro no IO");
			e.printStackTrace();
		}
		
		return resultado;
	}
	
	@Override
	protected void onPostExecute(JSONArray res )
	{
		if(res != null)
		{
			ArrayList<Grupo> gruposResultado = new ArrayList<Grupo>();
			
			for(int i = 0; i < res.length(); i++)
			{
				try 
				{
					Grupo g = new Grupo();
					
					g.setId(res.getJSONObject(i).getInt("idGrupo"));
					g.setNomeDoGrupo(res.getJSONObject(i).getString("nomeGrupo"));
					g.setAdmDoGrupo(res.getJSONObject(i).getString("admGrupo"));
					g.setDescDoGrupo(res.getJSONObject(i).getString("descricaoGrupo"));
					
					gruposResultado.add(g);
				} 
				catch (JSONException e) 
				{
					e.printStackTrace();
					Log.e("MinhaTag", "Erro ao montar o resultado de exercicios:" + res.toString());
				}
			}
			
			TelaMae.retornaPesquisa(gruposResultado);
		}
		else
		{
			TelaMae.naoRetornaPesquisa();
		}
	}

}
