package adapters;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import modelos.ItemExercicio;
import modelos.ItemRefeicao;
import modelos.Usuario;


import com.example.tcc.R;

import fragmentos.FragmentoTreinoExecucao;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.opengl.Visibility;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class ListaItemRefeicoesFinalAdapter extends ArrayAdapter<ItemRefeicao>
{
	private List<ItemRefeicao> itensDaListaR;
	private final Fragment TELA;
	
	public ListaItemRefeicoesFinalAdapter(Fragment activity, int textViewResourceId, List<ItemRefeicao> itensRefeicao)
	{
		super(activity.getActivity(), textViewResourceId, itensRefeicao);
		
		TELA = activity;
		itensDaListaR = itensRefeicao;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{		
		if(convertView == null)
		{
			convertView = TELA.getActivity().getLayoutInflater().inflate(R.layout.final_item_refeicao, null);
		}
		
		TextView txtNomeRefeicao = (TextView) convertView.findViewById(R.id.txtNomeRefeicaoFinal);
		txtNomeRefeicao.setText("" + itensDaListaR.get(position).getNomeRefeicao());
		
		String alimentos = "";
		
		TextView txtAlimentos = (TextView) convertView.findViewById(R.id.txtAlimentosSelecionadosFinal);
		
		for(int i = 0; i < itensDaListaR.get(position).getQuantidadeAlimentos(); i++)
		{
			alimentos += itensDaListaR.get(position).getAlimento(i).getNome() + "\n";
		}
		
		txtAlimentos.setText(alimentos);
		
		TextView txtHorario = (TextView) convertView.findViewById(R.id.txtHoraRefeicaoFinal);
		txtHorario.setText("" + itensDaListaR.get(position).getHorario());
		
		return convertView;
	}
	
	@Override
	public int getCount() 
	{
		return super.getCount();
	}
	
	@Override
	public ItemRefeicao getItem(int position) 
	{
		return itensDaListaR.get(position);
	}
}
