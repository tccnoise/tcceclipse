package adapters;


import fragmentos.FragmentoNovoTreino;
import fragmentos.FragmentoTreino;
import fragmentos.FragmentoTreinoExecucao;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class AdapterViewPagerTreinoExecucao extends FragmentStatePagerAdapter 
{

	public AdapterViewPagerTreinoExecucao(FragmentManager fm) 
	{
		super(fm);
	}

	@Override
	public Fragment getItem(int i) 
	{
		Fragment frag = null;
		switch (i)
		{
			case 0 :
				frag = new FragmentoTreinoExecucao("Seg");
				break;
			case 1 :
				frag = new FragmentoTreinoExecucao("Ter");
				break;
			case 2 :
				frag = new FragmentoTreinoExecucao("Qua");
				break;
			case 3 :
				frag = new FragmentoTreinoExecucao("Qui");
				break;
			case 4 :
				frag = new FragmentoTreinoExecucao("Sex");
				break;
			case 5 :
				frag = new FragmentoTreinoExecucao("Sab");
				break;
			case 6 :
				frag = new FragmentoTreinoExecucao("Dom");
				break;
		}
		
		return frag;
	}

	@Override
	public int getCount() 
	{
		return 7;
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		String s = "";
		
		if (position == 0)
		{
			s = "Seg";
		}
		else if(position == 1)
		{
			s = "Ter";
		}
		else if(position == 2)
		{
			s = "Qua";
		}
		else if(position == 3)
		{
			s = "Qui";
		}
		else if(position == 4)
		{
			s = "Sex";
		}
		else if(position == 5)
		{
			s = "Sab";
		}
		else
		{
			s = "Dom";
		}
		
		return s;
	}

}
