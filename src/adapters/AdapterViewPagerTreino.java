package adapters;


import fragmentos.FragmentoNovoTreino;
import fragmentos.FragmentoTreino;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class AdapterViewPagerTreino extends FragmentStatePagerAdapter 
{

	public AdapterViewPagerTreino(FragmentManager fm) 
	{
		super(fm);
	}

	@Override
	public Fragment getItem(int i) 
	{
		Fragment frag = null;
		switch (i)
		{
			case 0 :
				frag = new FragmentoTreino("Seg");
				break;
			case 1 :
				frag = new FragmentoTreino("Ter");
				break;
			case 2 :
				frag = new FragmentoTreino("Qua");
				break;
			case 3 :
				frag = new FragmentoTreino("Qui");
				break;
			case 4 :
				frag = new FragmentoTreino("Sex");
				break;
			case 5 :
				frag = new FragmentoTreino("Sab");
				break;
			case 6 :
				frag = new FragmentoTreino("Dom");
				break;
		}
		
		return frag;
	}

	@Override
	public int getCount() 
	{
		return 7;
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		String s = "";
		
		if (position == 0)
		{
			s = "Seg";
		}
		else if(position == 1)
		{
			s = "Ter";
		}
		else if(position == 2)
		{
			s = "Qua";
		}
		else if(position == 3)
		{
			s = "Qui";
		}
		else if(position == 4)
		{
			s = "Sex";
		}
		else if(position == 5)
		{
			s = "Sab";
		}
		else
		{
			s = "Dom";
		}
		
		return s;
	}

}
