package adapters;

import java.util.ArrayList;
import java.util.List;

import modelos.ItemExercicio;

import com.example.tcc.R;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListaAtributosAdapter extends ArrayAdapter<String> {

	private ArrayList<String> atributos;
	
	private ArrayList<Float> valores;
	
	public ListaAtributosAdapter(Context context, int resource, ArrayList<String> at, ArrayList<Float> va) {
		super(context, resource);
		
		atributos = at;
		valores = va;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if(convertView == null)
		{
			convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.item_medidas_perfil, null);
		}
		
		TextView atributo = (TextView) convertView.findViewById(R.id.txtAtributoPerfil);
		TextView valor = (TextView) convertView.findViewById(R.id.txtValorPerfil);
		
		atributo.setText(atributos.get(position) + ": ");
		valor.setText(valores.get(position).toString());
		
		return convertView;
	}
	
	@Override
	public int getCount() 
	{
		return atributos.size();
	}
	
	@Override
	public String getItem(int position) 
	{
		return atributos.get(position);
	}
}
