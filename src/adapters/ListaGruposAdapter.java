package adapters;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import modelos.Grupo;
import modelos.ItemExercicio;
import modelos.Usuario;


import com.example.tcc.R;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class ListaGruposAdapter extends ArrayAdapter<Grupo>
{
	private List<Grupo> listaGrupos;
	private final Activity TELA;
	//private int Dia;
	
	public ListaGruposAdapter(Activity activity, int textViewResourceId, List<Grupo> grupos)
	{
		super(activity, textViewResourceId, grupos);
		
		TELA = activity;
		listaGrupos = grupos;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{		
		if(convertView == null)
		{
			convertView = TELA.getLayoutInflater().inflate(R.layout.item_grupo, null);
		}
		
		TextView txtNome = (TextView) convertView.findViewById(R.id.txtNomeDoGrupo);
		txtNome.setText("" + listaGrupos.get(position).getNomeDoGrupo());
		
		TextView txtDesc = (TextView) convertView.findViewById(R.id.txtDescDoGrupo);
		txtDesc.setText("" + listaGrupos.get(position).getDescDoGrupo());
		
		TextView txtAdm = (TextView) convertView.findViewById(R.id.txtAdmDoGrupo);
		txtAdm.setText("" + listaGrupos.get(position).getAdmDoGrupo());
		
		return convertView;
	}
	
	@Override
	public int getCount() 
	{
		return super.getCount();
	}
	
	@Override
	public Grupo getItem(int position) 
	{
		return listaGrupos.get(position);
	}
}
