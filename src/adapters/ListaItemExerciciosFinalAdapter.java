package adapters;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import modelos.ItemExercicio;
import modelos.Usuario;


import com.example.tcc.R;

import fragmentos.FragmentoTreinoExecucao;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.opengl.Visibility;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class ListaItemExerciciosFinalAdapter extends ArrayAdapter<ItemExercicio>
{
	private List<ItemExercicio> itensDaListaE;
	private final Fragment TELA;
	
	public ListaItemExerciciosFinalAdapter(Fragment activity, int textViewResourceId, List<ItemExercicio> itensExercicio)
	{
		super(activity.getActivity(), textViewResourceId, itensExercicio);
		
		TELA = activity;
		itensDaListaE = itensExercicio;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{		
		if(convertView == null)
		{
			convertView = TELA.getActivity().getLayoutInflater().inflate(R.layout.final_item_exercicio, null);
		}
		
		TextView txtTipo = (TextView) convertView.findViewById(R.id.txtTipoExercicio);
		txtTipo.setText("" + itensDaListaE.get(position).getTipo());
		
		TextView txtExercicio = (TextView) convertView.findViewById(R.id.txtExercicio);
		txtExercicio.setText("" + itensDaListaE.get(position).getExercicio());
		
		TextView txtSerie = (TextView) convertView.findViewById(R.id.txtSeries);
		txtSerie.setText("" + itensDaListaE.get(position).getSeries());
		
		TextView txtRepeticao = (TextView) convertView.findViewById(R.id.txtRepeticao);
		txtRepeticao.setText("" + itensDaListaE.get(position).getRepeticoes());
		
		TextView txtDescanso = (TextView) convertView.findViewById(R.id.txtDescanso);
		txtDescanso.setText("" + itensDaListaE.get(position).getDescanso());
		
		/*if(TELA.getClass() == FragmentoTreinoExecucao.class)
		{
			LinearLayout l = (LinearLayout) convertView.findViewById(R.id.linearLayoutExercicios);
			final Activity aux = TELA.getActivity();
			
			l.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					LinearLayout l2 = (LinearLayout) aux.findViewById(R.id.layoutCronometro);
					l2.setVisibility(View.VISIBLE);
				}
			});
		}*/
		
		return convertView;
	}
	
	@Override
	public int getCount() 
	{
		return super.getCount();
	}
	
	@Override
	public ItemExercicio getItem(int position) 
	{
		return itensDaListaE.get(position);
	}
}
