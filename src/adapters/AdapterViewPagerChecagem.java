package adapters;

import fragmentos.FragmentoChecagem;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class AdapterViewPagerChecagem extends FragmentStatePagerAdapter 
{

	public AdapterViewPagerChecagem(FragmentManager fm) 
	{
		super(fm);
	}

	@Override
	public Fragment getItem(int i) 
	{
		Fragment frag = null;
		
		switch (i)
		{
			case 0:
				frag = new FragmentoChecagem("treino");
				break;
			case 1:
				frag = new FragmentoChecagem("alimentacao");
				break;
		}
		
		return frag;
	}

	@Override
	public int getCount() 
	{
		return 2;
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		String s = "";
		
		if (position == 0)
		{
			s = "TREINO";
		}
		else if(position == 1)
		{
			s = "ALIMENTACAO";
		}
		
		return s;
	}

}
