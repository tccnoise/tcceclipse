package adapters;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import modelos.ItemExercicio;
import modelos.Usuario;


import com.example.tcc.R;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class ListaItemExerciciosAdapter extends ArrayAdapter<ItemExercicio>
{
	private List<ItemExercicio> itensDaListaE;
	private final Activity TELA;
	//private int Dia;
	
	public ListaItemExerciciosAdapter(Activity activity, int textViewResourceId, List<ItemExercicio> itensExercicio/*, int dia*/)
	{
		super(activity, textViewResourceId, itensExercicio);
		
		TELA = activity;
		itensDaListaE = itensExercicio;
		//Dia = dia;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		/*SharedPreferences info = TELA.getSharedPreferences("Usuario", TELA.MODE_PRIVATE);
		
		Usuario usuario = new Usuario();
		
		try 
		{
			usuario.recuperaUsuarioJSON(new JSONObject(info.getString("usuario", "")));
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao recuperar o JSON do usuario na tela de novo treino posicao: " + position);
		}*/
		
		if(convertView == null)
		{
			convertView = TELA.getLayoutInflater().inflate(R.layout.item_exercicio, null);
		}
		
		Spinner spinTipo = (Spinner) convertView.findViewById(R.id.spinnerTipoExercicio);
		final String[] tipos = {"aerobico", "musculacao"};		
		ArrayAdapter<String> adaptadorTipo = new ArrayAdapter<String>(TELA, android.R.layout.simple_spinner_item, tipos);		
		spinTipo.setAdapter(adaptadorTipo);
		spinTipo.setSelection(encontraIndexPorTextoSpinner(spinTipo, itensDaListaE.get(position).getTipo()));
		//spinTipo.setSelection(encontraIndexPorTextoSpinner(spinTipo, usuario.getTreino(Dia).getItemExercicioDaLista(position).getTipo()));
		
		Spinner spinExercicio = (Spinner) convertView.findViewById(R.id.spinnerExercicio);
		final String[] exercicios = {"Supino", "Corrida", "Remador", "bicicleta"};
		SharedPreferences info = TELA.getSharedPreferences("Exercicios", TELA.MODE_PRIVATE);
        
        List<String> exerciciosLista = new ArrayList<String>(info.getStringSet("listaExercicios", null));
		ArrayAdapter<String> adaptadorExercicios = new ArrayAdapter<String>(TELA, android.R.layout.simple_spinner_item, exerciciosLista);		
		spinExercicio.setAdapter(adaptadorExercicios);
		spinExercicio.setSelection(encontraIndexPorTextoSpinner(spinExercicio, itensDaListaE.get(position).getExercicio()));
		//spinTipo.setSelection(encontraIndexPorTextoSpinner(spinTipo, usuario.getTreino(Dia).getItemExercicioDaLista(position).getExercicio()));
		
		EditText etxtSerie = (EditText) convertView.findViewById(R.id.edtxtSeries);
		etxtSerie.setText("" + itensDaListaE.get(position).getSeries());
		//etxtSerie.setText("" + usuario.getTreino(Dia).getItemExercicioDaLista(position).getSeries());
		
		EditText etxtRepeticao = (EditText) convertView.findViewById(R.id.edtxtRepeticao);
		etxtRepeticao.setText("" + itensDaListaE.get(position).getRepeticoes());
		//etxtRepeticao.setText("" + usuario.getTreino(Dia).getItemExercicioDaLista(position).getRepeticoes());
		
		EditText etxtDescanso = (EditText) convertView.findViewById(R.id.edtxtDescanso);
		etxtDescanso.setText("" + itensDaListaE.get(position).getDescanso());
		//etxtDescanso.setText("" + usuario.getTreino(Dia).getItemExercicioDaLista(position).getDescanso());
		
		return convertView;
	}
	
	@Override
	public int getCount() 
	{
		return super.getCount();
	}
	
	@Override
	public ItemExercicio getItem(int position) 
	{
		return itensDaListaE.get(position);
	}
	
	private int encontraIndexPorTextoSpinner(Spinner s, String txt)
	{
		int indexSpinner = 0;
		
		for(int i = 0; i < s.getCount(); i++)
		{
			if(s.getItemAtPosition(i).toString().equalsIgnoreCase(txt))
			{
				indexSpinner = i;
				break;
			}
		}
		return indexSpinner;
	}
}
