package adapters;

import java.util.ArrayList;
import java.util.List;

import modelos.Alimento;
import modelos.ItemRefeicao;


import com.example.tcc.R;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

public class AlimentosAdapter extends ArrayAdapter<Alimento>
{
	private List<Alimento> alimentos;
	private final Activity TELA;
	ArrayList<Boolean> checked = new ArrayList<Boolean>();
	ArrayList<String> porcaoNum = new ArrayList<String>();
	
	public AlimentosAdapter(Activity activity, int textViewResourceId, List<Alimento> ali)
	{
		super(activity, textViewResourceId, ali);
		
		TELA = activity;
		alimentos = ali;
		
		for(int i = 0; i < ali.size(); i++)
		{
			checked.add(false);
			porcaoNum.add("");
		}
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		
		if(convertView == null)
		{
			//Log.d("MinhaTag", "Vai criar uma view nova posicao: " + position);
			convertView = TELA.getLayoutInflater().inflate(R.layout.alimentos_escolha, null);			
		}
		
		CheckBox alimento = (CheckBox) convertView.findViewById(R.id.cb_alimento);
		
		alimento.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) 
			{
				if(isChecked)
				{
					checked.set(position, true);
				}
				else
				{
					checked.set(position, false);
				}
			}
		});
		alimento.setChecked(checked.get(position));
		alimento.setText(alimentos.get(position).getNome());
		
		return convertView;
	}
	
	@Override
	public int getCount() 
	{
		return alimentos.size();
	}
	
	@Override
	public Alimento getItem(int position) 
	{
		return alimentos.get(position);
	}
}
