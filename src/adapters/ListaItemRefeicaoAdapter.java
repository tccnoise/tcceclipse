package adapters;

import java.util.ArrayList;
import java.util.List;

import telas.TelaNovosAlimentos;

import modelos.ItemRefeicao;


import com.example.tcc.R;

import fragmentos.FragmentoNovaDieta;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.opengl.Visibility;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class ListaItemRefeicaoAdapter extends ArrayAdapter<ItemRefeicao>
{
	private List<ItemRefeicao> itensDaListaRef;
	private final Activity TELA;
	private String dia;
	final FragmentoNovaDieta frag;

	
	public ListaItemRefeicaoAdapter(FragmentoNovaDieta activity, int textViewResourceId, List<ItemRefeicao> itensRefeicao, String d)
	{
		super(activity.getActivity(), textViewResourceId, itensRefeicao);
		
		TELA = activity.getActivity();
		frag = activity;
		itensDaListaRef = itensRefeicao;
		dia = d;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if(convertView == null)
		{
			convertView = TELA.getLayoutInflater().inflate(R.layout.item_refeicao, null);
		}
		
		ArrayList<String> nomeRefeicoes = new ArrayList<String>();
		
		nomeRefeicoes.add("Cafe da Manha");nomeRefeicoes.add("Almoco");nomeRefeicoes.add("Lanche");nomeRefeicoes.add("Janta");
		nomeRefeicoes.add("Sobremesa");nomeRefeicoes.add("Refeicao");
		
		Spinner spNomeRef = (Spinner) convertView.findViewById(R.id.spinnerNomeRefeicao);
		spNomeRef.setAdapter(new ArrayAdapter<String>(TELA, android.R.layout.simple_spinner_item, nomeRefeicoes));
		spNomeRef.setSelection(encontraIndexPorTextoSpinner(spNomeRef, itensDaListaRef.get(position).getNomeRefeicao()));
		
		TextView txtAlimentos = (TextView) convertView.findViewById(R.id.txtAlimentosSelecionados);
		//etxtNomeRef.setText(itensDaListaRef.get(position).getNomeRefeicao());
		
		if(itensDaListaRef.get(position).getQuantidadeAlimentos() > 0)
		{
			if(itensDaListaRef.get(position).getAlimento(0).getId() != -1)
			{
				String nomeAlimentos = "";
				
				for(int i = 0; i < itensDaListaRef.get(position).getQuantidadeAlimentos(); i++)
				{
					nomeAlimentos += itensDaListaRef.get(position).getAlimento(i).getNome() + " ";
				}
				
				txtAlimentos.setText(nomeAlimentos.trim());
				txtAlimentos.setVisibility(View.VISIBLE);
			}
		}
		
		Button btnAlimentos = (Button) convertView.findViewById(R.id.btnAlimentos);
		
		final int i = position;
		
		btnAlimentos.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				frag.salvaValoresTemporarios();
				Intent in = new Intent(TELA.getApplicationContext(), TelaNovosAlimentos.class);
				in.putExtra("identificador", dia + i);
				TELA.startActivity(in);				
			}
		});
		
		//EditText etxtAlimento = (EditText) convertView.findViewById(R.id.edtxtAlimento);
		//etxtAlimento.setText(itensDaListaRef.get(position).getAlimento());
		
		ArrayList<String> horarios = new ArrayList<String>();
		
		horarios.add("5:00");horarios.add("5:30");horarios.add("6:00");horarios.add("6:30");
		horarios.add("7:00");horarios.add("7:30");horarios.add("8:00");horarios.add("8:30");horarios.add("9:00");horarios.add("9:30");
		horarios.add("10:00");horarios.add("10:30");horarios.add("11:00");horarios.add("12:30");horarios.add("13:00");horarios.add("13:30");
		horarios.add("14:00");horarios.add("14:30");horarios.add("15:00");horarios.add("15:30");horarios.add("16:00");horarios.add("16:30");
		horarios.add("17:00");horarios.add("17:30");horarios.add("18:00");horarios.add("18:30");horarios.add("19:00");horarios.add("19:30");
		horarios.add("20:00");horarios.add("20:30");horarios.add("21:00");horarios.add("21:30");horarios.add("22:00");horarios.add("22:30");
		horarios.add("23:00");horarios.add("23:30");horarios.add("00:00");horarios.add("00:30");horarios.add("01:00");horarios.add("01:30");
		
		Spinner spHorario = (Spinner) convertView.findViewById(R.id.spinnerHoraRefeicao);
		spHorario.setAdapter(new ArrayAdapter<String>(TELA, android.R.layout.simple_spinner_item, horarios));
		spHorario.setSelection(encontraIndexPorTextoSpinner(spHorario, itensDaListaRef.get(position).getHorario()));
		//etxtHorario.setText(itensDaListaRef.get(position).getHorario());
		
		/*EditText etxtPorcao = (EditText) convertView.findViewById(R.id.edtxtPorcao);
		etxtPorcao.setText("" + itensDaListaRef.get(position).getPorcao());*/
		
		return convertView;
	}
	
	@Override
	public int getCount() 
	{
		return super.getCount();
	}
	
	@Override
	public ItemRefeicao getItem(int position) 
	{
		return itensDaListaRef.get(position);
	}
	
	private int encontraIndexPorTextoSpinner(Spinner s, String txt)
	{
		int indexSpinner = 0;
		
		for(int i = 0; i < s.getCount(); i++)
		{
			if(s.getItemAtPosition(i).toString().equalsIgnoreCase(txt))
			{
				indexSpinner = i;
				break;
			}
		}
		return indexSpinner;
	}
}
