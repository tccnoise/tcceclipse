package adapters;

import java.util.ArrayList;

import com.example.tcc.R;

import telas.SpinnerActionBarItem;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterSpinnerActionBarItem extends BaseAdapter
{
	private ImageView img;
	private TextView titulo;
	private ArrayList<SpinnerActionBarItem> itens;
	private Context ctx;
	
	public AdapterSpinnerActionBarItem(Context t, ArrayList<SpinnerActionBarItem> a)
	{
		ctx = t;
		itens = a;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return itens.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return itens.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup pai) {
		// TODO Auto-generated method stub
		if(convertView == null){
			LayoutInflater l = (LayoutInflater) ctx.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = l.inflate(R.layout.spinner_actionbar_item, null);
		}
		
		img = (ImageView) convertView.findViewById(R.id.ImgSpinner);
		titulo = (TextView) convertView.findViewById(R.id.TextoSpinner);
		
		img.setImageResource(itens.get(position).getIcon());
		img.setVisibility(View.GONE);
		
		titulo.setText(itens.get(position).getTitulo());
				
		return convertView;
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup p){
		if(convertView == null){
			LayoutInflater l = (LayoutInflater) ctx.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = l.inflate(R.layout.spinner_actionbar_item_lista, null);
		}
		
		img = (ImageView) convertView.findViewById(R.id.ImgSpinner);
		titulo = (TextView) convertView.findViewById(R.id.TextoSpinner);
		
		img.setImageResource(itens.get(position).getIcon());
		
		titulo.setText(itens.get(position).getTitulo());
				
		return convertView;
	}
}
