package adapters;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import modelos.Grupo;
import modelos.ItemExercicio;
import modelos.Usuario;
import modelos.UsuarioGrupo;


import com.example.tcc.R;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class ListaUsuariosGrupoAdapter extends ArrayAdapter<UsuarioGrupo>
{
	private List<UsuarioGrupo> listaUsuarios;
	private final Activity TELA;
	//private int Dia;
	
	public ListaUsuariosGrupoAdapter(Activity activity, int textViewResourceId, List<UsuarioGrupo> u)
	{
		super(activity, textViewResourceId, u);
		
		TELA = activity;
		listaUsuarios = u;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{		
		if(convertView == null)
		{
			convertView = TELA.getLayoutInflater().inflate(R.layout.usuarios_grupo, null);
		}
		
		TextView txtNome = (TextView) convertView.findViewById(R.id.txtNomeUsuarioGrupo);
		txtNome.setText(listaUsuarios.get(position).getNomeUsuarioGrupo());
		
		TextView txtPontos = (TextView) convertView.findViewById(R.id.txtPontosUsuarioGrupo);
		txtPontos.setText("" + listaUsuarios.get(position).getPontosUsuarioGrupo());
		
		return convertView;
	}
	
	@Override
	public int getCount() 
	{
		return listaUsuarios.size();
	}
	
	@Override
	public UsuarioGrupo getItem(int position) 
	{
		return listaUsuarios.get(position);
	}
}
