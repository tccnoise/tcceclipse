package com.example.tcc;

import java.util.ArrayList;

import modelos.Alimento;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import telas.TelaBaseDieta;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

public class PesquisaAlimentos extends TelaBaseDieta {
	
	Spinner atributo;
	Spinner equacao;
	
	EditText valor;
	
	Button btnPesquisa;
	
	ListView resultado;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pesquisa_alimentos);
		
		atributo = (Spinner) findViewById(R.id.spinnerParametroAlimento);
		equacao = (Spinner) findViewById(R.id.spinnerMaiorMenor);
		
		valor = (EditText) findViewById(R.id.edTxtParametroAlimento);
		btnPesquisa = (Button) findViewById(R.id.btnPesquisaAlimento);
		resultado = (ListView) findViewById(R.id.listViewResultadoAlimentos);
		
		String [] parametros = {"","calorias","carboidratos","proteinas","gorduras totais","gorduras saturadas","gorduras trans","fibras","sodio"};
		atributo.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, parametros));
		
		String [] maismenos = {"","maior que","menor que"};
		equacao.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, maismenos));
		
		btnPesquisa.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(atributo.getSelectedItemPosition() != 0 && equacao.getSelectedItemPosition() != 0 && !valor.getText().toString().trim().equals(""))
				{
					pesquisaAlimentosFiltros(atributo.getSelectedItemPosition(), equacao.getSelectedItemPosition());
				}
			}
		});
	}
	
	public void pesquisaAlimentosFiltros(int at, int eq)
	{
		ArrayList<Alimento> al = new ArrayList<Alimento>();
		
		SharedPreferences c;
		
		c = getSharedPreferences("Alimentos", MODE_PRIVATE);
		
		String alimentosString = c.getString("listaAlimentos", "N�o encontrou nenhum alimento");
		
		try 
		{
			JSONArray alimentosJSON = new JSONArray(alimentosString);
			
			int i = 0;

			while(i < alimentosJSON.length())
			{
				JSONObject aJson = alimentosJSON.getJSONObject(i);
				Alimento a = new Alimento();
				
				a.recuperaAlimentoJSON(aJson);
				al.add(a);
				
				i++;
			}
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Erro na Pesquisa de alimentos ao recuperar o jsonarray");
		}
		
		ArrayList<String> resultadoA = new ArrayList<String>();
		
		float v = Float.parseFloat(valor.getText().toString());
		
		switch(at)
		{
			case 1:
				if(eq == 1)
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getCaloria() > v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				else
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getCaloria() < v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				break;
				
			case 2:
				if(eq == 1)
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getCarboidratos() > v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				else
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getCarboidratos() < v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				break;
				
			case 3:
				if(eq == 1)
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getProteinas() > v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				else
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getProteinas() < v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				break;
				
			case 4:
				if(eq == 1)
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getGordurasTotais() > v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				else
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getGordurasTotais() < v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				break;
				
			case 5:
				if(eq == 1)
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getSaturadas() > v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				else
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getSaturadas() < v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				break;
				
			case 6:
				if(eq == 1)
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getGordurasTrans() > v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				else
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getGordurasTrans() < v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				break;
				
			case 7:
				if(eq == 1)
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getFibras() > v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				else
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getFibras() < v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				break;
				
			case 8:
				if(eq == 1)
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getSodio() > v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				else
				{
					for(int i = 0; i < al.size(); i++)
					{
						if(al.get(i).getSodio() < v)
						{
							resultadoA.add(al.get(i).getNome());
						}
					}
				}
				break;
		}
		
		resultado.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, resultadoA));
	}
}
