package com.example.tcc;

import org.json.JSONException;
import org.json.JSONObject;

import modelos.Grupo;
import telas.TelaBaseGrupo;
import adapters.ListaUsuariosGrupoAdapter;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

public class TelaGrupoInformacoes extends TelaBaseGrupo 
{
	TextView nomeGrupo;
	TextView admGrupo;
	TextView descGrupo;
	
	ListView listaDeUsuarios;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_grupo_informacoes);
		
		nomeGrupo = (TextView) findViewById(R.id.txtNomeGrupoInfo);
		admGrupo = (TextView) findViewById(R.id.txtAdmGrupoInfo);
		descGrupo = (TextView) findViewById(R.id.txtDescGrupoInfo);
		
		listaDeUsuarios = (ListView) findViewById(R.id.listViewUsuariosDoGrupo);
		
		Bundle extras = getIntent().getExtras();
		
		Grupo grupo = new Grupo();
		
		if(extras == null)
		{
			Log.e("MinhaTag", "Sem nenhum valor na tela de Informacao de um grupo");
		}
		else
		{
			try 
			{
				grupo.recuperaGrupoJSON(new JSONObject(extras.getString("grupo")));
				
				nomeGrupo.setText(grupo.getNomeDoGrupo());
				admGrupo.setText(grupo.getAdmDoGrupo());
				descGrupo.setText(grupo.getDescDoGrupo());
				
				listaDeUsuarios.setAdapter(new ListaUsuariosGrupoAdapter(this, R.layout.usuarios_grupo, grupo.getListaDeUsuarios()));
				
			} 
			catch (JSONException e) 
			{
				e.printStackTrace();
				Log.e("MinhaTag", "Erro ao recuperar um grupo de json na tela de Informacao de um grupo");
			}
		}
		
		
	}
}
