package util;

import android.view.View;
import android.widget.ListView;

public class UtilView 
{
	public View getViewPelaPosicao(int pos, ListView listView)
	{
		final int primeiraPosicao = listView.getFirstVisiblePosition();
	    final int ultimaPosicao = primeiraPosicao + listView.getChildCount() - 1;

	    if (pos < primeiraPosicao || pos > ultimaPosicao ) 
	    {
	        return listView.getAdapter().getView(pos, null, listView);
	    } 
	    else 
	    {
	        final int childIndex = pos - primeiraPosicao;
	        return listView.getChildAt(childIndex);
	    }
	}
	
	public int numeroDiaDaSemana(String dia)
	{
		int numero = 0;
		
		if(dia == "Seg")
		{
			numero = 0;
		}
		else if(dia == "Ter")
		{
			numero = 1;
		}
		else if(dia == "Qua")
		{
			numero = 2;
		}
		else if(dia == "Qui")
		{
			numero = 3;
		}
		else if(dia == "Sex")
		{
			numero = 4;
		}
		else if(dia == "Sab")
		{
			numero = 5;
		}
		else if(dia == "Dom")
		{
			numero = 6;
		}
		
		return numero;
	}
	
	public String stringDiaDaSemana(int dia)
	{
		String diaDaSemana = "";
		
		if(dia == 0)
		{
			diaDaSemana = "Seg";
		}
		else if(dia == 1)
		{
			diaDaSemana = "Ter";
		}
		else if(dia == 2)
		{
			diaDaSemana = "Qua";
		}
		else if(dia == 3)
		{
			diaDaSemana = "Qui";
		}
		else if(dia == 4)
		{
			diaDaSemana = "Sex";
		}
		else if(dia == 5)
		{
			diaDaSemana = "Sab";
		}
		else if(dia == 6)
		{
			diaDaSemana = "Dom";
		}
		
		return diaDaSemana;
	}
}
