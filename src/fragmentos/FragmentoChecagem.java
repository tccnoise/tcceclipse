package fragmentos;

import org.json.JSONException;
import org.json.JSONObject;

import webservice.EnviaPontuacaoDiaria;

import modelos.Usuario;

import com.example.tcc.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentoChecagem extends Fragment 
{
	String modo;
	
	public FragmentoChecagem(String tipo)
	{
		modo = tipo;
	}
	@Override
	public View onCreateView(LayoutInflater i, ViewGroup C, Bundle dadoSalvo)
	{
		final Activity aux = this.getActivity();
		
		final View v = i.inflate(R.layout.tela_validacao, C, false);
		
		//TextView titulo = (TextView) v.findViewById(R.id.titulo_checagem_diaria);
		TextView texto = (TextView) v.findViewById(R.id.texto_checagem_diaria);
		
		final RadioGroup rg = (RadioGroup) v.findViewById(R.id.radioGroupChecagem);
		Button btnEnviar = (Button) v.findViewById(R.id.btn_enviarChecagem);
		
		final String tipo = modo;
		
		SharedPreferences c;
		
		c = v.getContext().getSharedPreferences("Checagem", v.getContext().MODE_PRIVATE);
		
		Editor edit = c.edit();
		edit.remove(tipo);
		
		edit.commit();
		
		if(modo.equals("treino"))
		{
			//titulo.setText("TREINO");
			texto.setText(R.string.Desc_Texto_Checagem_Treino);
		}
		else
		{
			//titulo.setText("ALIMENTACAO");
			texto.setText(R.string.Desc_Texto_Checagem_Alimentacao);
		}
		
		rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup grupo, int checkedId) {
				int nota = 0;
				
				switch (checkedId)
				{
					case R.id.radioBtn_10:
						nota = 10;
						break;
					case R.id.radioBtn_9:
						nota = 9;
						break;
					case R.id.radioBtn_8:
						nota = 8;
						break;
					case R.id.radioBtn_7:
						nota = 7;
						break;
					case R.id.radioBtn_6:
						nota = 6;
						break;
					case R.id.radioBtn_5:
						nota = 5;
						break;
					case R.id.radioBtn_4:
						nota = 4;
						break;
					case R.id.radioBtn_3:
						nota = 3;
						break;
					case R.id.radioBtn_2:
						nota = 2;
						break;
					case R.id.radioBtn_1:
						nota = 1;
						break;
					case R.id.radioBtn_0:
						nota = 0;
						break;
				}
				
				SharedPreferences c;
				
				c = v.getContext().getSharedPreferences("Checagem", v.getContext().MODE_PRIVATE);
				
				Editor edit = c.edit();
				edit.putInt(tipo, nota);
				
				edit.commit();
			}
		});
		
		btnEnviar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				SharedPreferences c;
				
				c = v.getContext().getSharedPreferences("Checagem", v.getContext().MODE_PRIVATE);
				
				if(c.contains("treino") && c.contains("alimentacao"))
				{
					int notaT = c.getInt("treino", -1);
					int notaA = c.getInt("alimentacao", -1);
					
					float resultado = ((float)(notaT + notaA)) / 2;
					
					Usuario usuario = new Usuario();
					
					c = v.getContext().getSharedPreferences("Usuario", v.getContext().MODE_PRIVATE);
					
					try 
					{
						usuario.recuperaUsuarioJSON(new JSONObject(c.getString("usuario", "Erro ao recuperar o usuario na tela de checagem")));
					} catch (JSONException e) {
						e.printStackTrace();
						Log.e("MinhaTag", "Erro ao recuperar um usuario de um json na checagem");
					}
					
					if(resultado == 10)
					{						
						usuario.setPontos(usuario.getPontos() + resultado + usuario.getBonus());
						usuario.setBonus(usuario.getBonus() + 0.1f);
					}
					else
					{
						usuario.setBonus(0);
						
						usuario.setPontos(usuario.getPontos() + resultado);
					}
					
					EnviaPontuacaoDiaria task = new EnviaPontuacaoDiaria(aux, usuario);
					task.execute();
				}
				else
				{
					if(c.contains("treino"))
					{
						Toast.makeText(v.getContext(), "� necessario configurar a alimenta��o", Toast.LENGTH_LONG).show();
					}
					else if(c.contains("alimentacao"))
					{
						Toast.makeText(v.getContext(), "� necessario configurar o treino", Toast.LENGTH_LONG).show();
					}
					else
					{
						Toast.makeText(v.getContext(), "� necessario configurar o treino e a alimenta��o", Toast.LENGTH_LONG).show();
					}
				}
			}
		});
		
		return v;
	}
}
