package fragmentos;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import telas.TelaNovosAlimentos;
import util.UtilView;
import webservice.EnviaDietaDiaDaSemana;
import webservice.EnviaTreinoDiaDaSemana;

import modelos.Alimento;
import modelos.Dieta;
import modelos.ItemRefeicao;
import modelos.Usuario;


import com.example.tcc.R;

import adapters.ListaItemRefeicaoAdapter;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

public class FragmentoNovaDieta extends Fragment 
{
	
	private ListView listaDeRefeicoes;
	private Button btnAdiciona;
	private Button btnRemove;
	private Button btnOk;
	
	//private ArrayList<ItemRefeicao> itemRef = new ArrayList<ItemRefeicao>();
	private Dieta dietaDia = new Dieta();
	
	private String dia;
	
	public FragmentoNovaDieta(String d)
	{
		dia = d;
	}
	
	@Override
	public View onCreateView(LayoutInflater i, ViewGroup C, Bundle dadoSalvo)
	{
		UtilView util = new UtilView();
		
		SharedPreferences info = getActivity().getSharedPreferences("Usuario", getActivity().MODE_PRIVATE);
		
		Usuario usuario = new Usuario();
		
		try 
		{
			usuario.recuperaUsuarioJSON(new JSONObject(info.getString("usuario", "Usuario nao encontrado ao tentar recuperar uma dieta" + dia)));
		} 
		catch (JSONException e1) 
		{
			e1.printStackTrace();
			Log.e("MinhaTag", "Erro ao tentar recuperar um usuario em uma nova dieta" + dia);
		}
		
		dietaDia = usuario.getDieta(util.numeroDiaDaSemana(dia));
		//dietaDia.DebugaDados();		
		
		if(info.contains("Dieta" + dia))
		{
			Log.d("MinhaTag", "Ta recuperando um valor ja existente" + dia);
			JSONArray dietaJSON = null;
			
			//Log.d("MinhaTag", "Dieta recuperada: " + info.getString("Dieta" + dia, "Nao encontrou dieta " + dia));
			try 
			{
				dietaJSON = new JSONArray(info.getString("Dieta" + dia, "Nao encontrou dieta " + dia));
			} 
			catch (JSONException e) 
			{
				e.printStackTrace();
				Log.e("MinhaTag", "Erro ao recuperar uma dieta no inicio do dia " + dia);
			}
			
			dietaDia.recuperaDietaJSON(dietaJSON);
			
			int index = 0;
			
			ArrayList<ItemRefeicao> lista = new ArrayList<ItemRefeicao>();
			
			while(info.contains(dia + index))
			{
				Log.i("MinhaTag", "contem index: " + index);
				ItemRefeicao item = new ItemRefeicao();
				
				item.setNomeRefeicao(dietaDia.getItemRefeicaoDaLista(index).getNomeRefeicao());
				item.setHorario(dietaDia.getItemRefeicaoDaLista(index).getHorario());
				
				ArrayList<Alimento> arrAl = new ArrayList<Alimento>();
				
				try 
				{
					JSONArray ja = new JSONArray(info.getString(dia + index, "Nao encontrou uma chave de um item refeicao " + dia));
					
					for(int count = 0; count < ja.length(); count++)
					{
						Alimento al = new Alimento();
						
						al.recuperaAlimentoJSON(ja.getJSONObject(count));
						arrAl.add(al);
					}
					
					item.setAlimentos(arrAl);
					dietaDia.getListaRefeicoes().set(index, item);
				} 
				catch (JSONException e) 
				{
					e.printStackTrace();
					Log.e("MinhaTag", "Erro ao recuperar um ItemRefeicao em um fragmento " + dia);
				}
				
				index++;
			}
		}
		
		View v = i.inflate(R.layout.fragment_nova_dieta, C, false);
		
		listaDeRefeicoes = (ListView) v.findViewById(R.id.listaRefeicoes);
        btnAdiciona = (Button) v.findViewById(R.id.btnAddRef);
        btnRemove = (Button) v.findViewById(R.id.btnDelRef);
        btnOk = (Button) v.findViewById(R.id.btnOkRef);
        
        montaElementosListView();
		
		return v;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) 
	{
		//Log.d("MinhaTag", "Vai salvar dados");
		salvaValoresTemporarios();
		super.onSaveInstanceState(outState);
	}
	
	private void montaElementosListView()
	{
		/*if(itemRef.size() == 0)
		{
			itemRef.add(new ItemRefeicao());
		}*/
		Log.d("MinhaTag", "LISTA TAMANHO: " + dietaDia.getListaRefeicoes().size());
		if(dietaDia.getListaRefeicoes().size() == 0)
		{
			dietaDia.getListaRefeicoes().add(new ItemRefeicao());
		}
        
        ArrayAdapter<ItemRefeicao> adapter = new ListaItemRefeicaoAdapter(this, R.layout.item_refeicao, dietaDia.getListaRefeicoes(), dia);
        
        listaDeRefeicoes.setAdapter(adapter);
        
        final FragmentoNovaDieta aux = this;
        
        btnAdiciona.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				salvaValoresTemporarios();
				dietaDia.getListaRefeicoes().add(new ItemRefeicao());
		        
		        ArrayAdapter<ItemRefeicao> adapter = new ListaItemRefeicaoAdapter(aux, R.layout.item_refeicao, dietaDia.getListaRefeicoes(), dia);
		        
		        listaDeRefeicoes.setAdapter(adapter);
			}
		});
        
        btnRemove.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				if(dietaDia.getListaRefeicoes().size() > 0)
				{
					salvaValoresTemporarios();
					
					SharedPreferences c = getActivity().getSharedPreferences("Usuario", getActivity().MODE_PRIVATE);
					Editor edit = c.edit();
					
					edit.remove(dia + (dietaDia.getListaRefeicoes().size() - 1));
					
					edit.commit();
					
					dietaDia.getListaRefeicoes().remove(dietaDia.getListaRefeicoes().get(dietaDia.getListaRefeicoes().size() - 1));
			        
			        ArrayAdapter<ItemRefeicao> adapter = new ListaItemRefeicaoAdapter(aux, R.layout.item_refeicao, dietaDia.getListaRefeicoes(), dia);
			        
			        listaDeRefeicoes.setAdapter(adapter);
				}
			}
		});
        
        btnOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				mandaDadosDietaServidor();
			}
		});
	}
	
	public void salvaValoresTemporarios()
	{
		UtilView util = new UtilView();
		SharedPreferences info = getActivity().getSharedPreferences("Usuario", getActivity().MODE_PRIVATE);
		Editor edit = info.edit();
		
		//Dieta dieta = new Dieta();
		
		boolean salva = true;
		for(int i = 0; i < dietaDia.getListaRefeicoes().size(); i++)
		{
			View v = util.getViewPelaPosicao(i, listaDeRefeicoes);
			Spinner nomeRefeicao = (Spinner) v.findViewById(R.id.spinnerNomeRefeicao);
			Spinner horario = (Spinner) v.findViewById(R.id.spinnerHoraRefeicao);
			
			dietaDia.getListaRefeicoes().get(i).setNomeRefeicao(nomeRefeicao.getSelectedItem().toString());
			
			if(info.contains(dia + i))
			{
				try 
				{
					Log.d("MinhaTag", "Valor q ta dando bosta: " + info.getString(dia + i, "erro ao recuperar a chave na nova dieta"));
					JSONArray ja = new JSONArray(info.getString(dia + i, "erro ao recuperar a chave na nova dieta"));
					
					ArrayList<Alimento> alim = new ArrayList<Alimento>();
					
					for(int j = 0; j < ja.length(); j++)
					{
						Alimento a = new Alimento();
						a.recuperaAlimentoJSON(ja.getJSONObject(j));
						
						alim.add(a);
					}
					
					dietaDia.getListaRefeicoes().get(i).setAlimentos(alim);
				} 
				catch (JSONException e) 
				{
					e.printStackTrace();
					Log.e("MinhaTag", "Erro ao tentar salvar os valores temporarios fragmento nova dieta dia: " + dia);
				}
			}
			else
			{
				if(dietaDia.getListaRefeicoes().size() <= 1)
				{
					Log.d("MinhaTag", "Chave nao existe e a lista tem um ou menos elementos");
					if(dietaDia.getListaRefeicoes().size() > 0)
					{
						if(dietaDia.getListaRefeicoes().get(0).getAlimento(0).getId() == -1)
						{
							Log.d("MinhaTag", "Chave nao existe e a lista tem um elemento padrao que nao deve ser salvo");
							salva = false;
						}
					}
					else
					{
						Log.d("MinhaTag", "Chave nao existe e a lista nao tem nenhum elemento");
						salva = false;
					}
				}
			}
			
			dietaDia.getListaRefeicoes().get(i).setHorario(horario.getSelectedItem().toString());
			
			//dieta.AdicionaItemRefeicaoNaLista(itemRef.get(i));
		}
		
		if(salva)
		{
			edit.putString("Dieta" + dia, dietaDia.toJSON().toString());
			
			edit.commit();
			
			//Log.d("MinhaTag", "Yuji qr saber: " + info.getString("Dieta" + dia, "nao EnControu dieta"));
		}
	}
	
	private void mandaDadosDietaServidor()
	{
		salvaValoresTemporarios();
		
		Usuario usuario = new Usuario();
		
		JSONObject json = new JSONObject();
		
		SharedPreferences info = getActivity().getSharedPreferences("Usuario", getActivity().MODE_PRIVATE);
		
		try 
		{
			usuario.recuperaUsuarioJSON(new JSONObject(info.getString("usuario", "Deu errado no mandadietaservidor")));
		} catch (JSONException e1) {
			e1.printStackTrace();
			Log.e("MinhaTag", "Erro ao recuperar o usuario de um JSON na hora de mandar um treino pro servidor");
		}
        
        try 
        {
			json.put("fk_usuario", usuario.getID());
			json.put("dia", dia);
			json.put("dieta", dietaDia.toJSON());
		} 
        catch (JSONException e) 
        {
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao tentar converter o json no fragmento nova dieta " + dia);
		}
        
        UtilView util = new UtilView();
        
        usuario.setDieta(dietaDia, util.numeroDiaDaSemana(dia));
        
        Log.d("MinhaTag", "ArrayJSON gerado no clique do botao OK: " + json.toString());
        EnviaDietaDiaDaSemana task = new EnviaDietaDiaDaSemana(this, json, usuario);
        task.execute();
	}
}
