package fragmentos;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.tcc.R;
import com.example.tcc.R.id;
import com.example.tcc.R.layout;

import util.UtilView;

import modelos.ItemExercicio;
import modelos.ItemRefeicao;
import modelos.Usuario;
import adapters.ListaItemExerciciosAdapter;
import adapters.ListaItemExerciciosFinalAdapter;
import adapters.ListaItemRefeicoesFinalAdapter;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class FragmentoDieta extends Fragment 
{
	private String dia;
	
	private ArrayList<ItemRefeicao> itemRef = new ArrayList<ItemRefeicao>();
	
	ListView listaDeExercicios;
	
	public FragmentoDieta(String dia)
	{
		this.dia = dia;
	}
	
	@Override
	public View onCreateView(LayoutInflater i, ViewGroup C, Bundle dadoSalvo)
	{
		SharedPreferences info = getActivity().getSharedPreferences("Usuario", Activity.MODE_PRIVATE);
			
		Usuario usuario = new Usuario();
			
		try 
		{
			usuario.recuperaUsuarioJSON(new JSONObject(info.getString("usuario", "")));
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao recuperar o JSON do usuario na tela de novo treino dia: " + dia);
		}
			
		UtilView util = new UtilView();
		
		int diaNumero = util.numeroDiaDaSemana(dia);
			
		for(int x = 0; x < usuario.getDieta(diaNumero).getNumeroDeRefeicoesNotreino(); x++)
		{
			ItemRefeicao refeicao = new ItemRefeicao();
				
			refeicao = usuario.getDieta(diaNumero).getItemRefeicaoDaLista(x);
				
			itemRef.add(refeicao);
		}
		
		View v = i.inflate(R.layout.fragmento_treino, C, false);
		
		listaDeExercicios = (ListView) v.findViewById(R.id.listViewExerciciosFinais);
		
		ArrayAdapter<ItemRefeicao> adapter = new ListaItemRefeicoesFinalAdapter(this, R.layout.final_item_exercicio, itemRef);
		
		listaDeExercicios.setAdapter(adapter);
		
		return v;
	}
}
