package fragmentos;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import util.UtilView;
import webservice.EnviaTreinoDiaDaSemana;

import modelos.ItemExercicio;
import modelos.Usuario;


import com.example.tcc.R;

import adapters.ListaItemExerciciosAdapter;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

public class FragmentoNovoTreino extends Fragment 
{
	
	private ListView listaDeExercicios;
	private Button btnAdiciona;
	private Button btnRemove;
	private Button btnOk;
	
	private String dia;
	
	private ArrayList<ItemExercicio> itemExe = new ArrayList<ItemExercicio>();
	
	public FragmentoNovoTreino(String dia)
	{
		this.dia = dia;
	}
	
	@Override
	public View onCreateView(LayoutInflater i, ViewGroup C, Bundle dadoSalvo)
	{
		if(dadoSalvo != null)
		{
			itemExe = dadoSalvo.getParcelableArrayList("ListaItensExercicios");
		}
		else
		{
			SharedPreferences info = getActivity().getSharedPreferences("Usuario", Activity.MODE_PRIVATE);
			
			Usuario usuario = new Usuario();
			
			try 
			{
				usuario.recuperaUsuarioJSON(new JSONObject(info.getString("usuario", "")));
			} catch (JSONException e) {
				e.printStackTrace();
				Log.e("MinhaTag", "Erro ao recuperar o JSON do usuario na tela de novo treino dia: " + dia);
			}
			
			UtilView util = new UtilView();
			
			int diaNumero = util.numeroDiaDaSemana(dia);
			
			for(int x = 0; x < usuario.getTreino(diaNumero).getNumeroDeExerciciosNotreino(); x++)
			{
				ItemExercicio exer = new ItemExercicio();
				
				exer = usuario.getTreino(diaNumero).getItemExercicioDaLista(x);
				
				itemExe.add(exer);
			}
		}
		
		View v = i.inflate(R.layout.fragment_novo_treino, C, false);
		
		listaDeExercicios = (ListView) v.findViewById(R.id.listaExercicios);
        btnAdiciona = (Button) v.findViewById(R.id.btnAddExer);
        btnRemove = (Button) v.findViewById(R.id.btnDelExer);
        btnOk = (Button) v.findViewById(R.id.btnOkExer);
        
        montaElementosListView();
		
		return v;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) 
	{
		SalvaValoresTemporarios();
		outState.putParcelableArrayList("ListaItensExercicios", itemExe);
        super.onSaveInstanceState(outState);
	}
	
	private void montaElementosListView()
	{
		if(itemExe.size() == 0)
		{
			itemExe.add(new ItemExercicio());
		}
        
        ArrayAdapter<ItemExercicio> adapter = new ListaItemExerciciosAdapter(this.getActivity(), R.layout.item_exercicio, itemExe);
        
        listaDeExercicios.setAdapter(adapter);
        
        final Activity aux = this.getActivity();
        
        btnAdiciona.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				SalvaValoresTemporarios();
				itemExe.add(new ItemExercicio());
		        
		        ArrayAdapter<ItemExercicio> adapter = new ListaItemExerciciosAdapter(aux, R.layout.item_exercicio, itemExe);
		        
		        listaDeExercicios.setAdapter(adapter);
			}
		});
        
        btnRemove.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				if(itemExe.size() > 0)
				{
					SalvaValoresTemporarios();
					itemExe.remove(itemExe.size() - 1);
			        
			        ArrayAdapter<ItemExercicio> adapter = new ListaItemExerciciosAdapter(aux, R.layout.item_exercicio, itemExe);
			        
			        listaDeExercicios.setAdapter(adapter);
				}
			}
        });
        
        btnOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				MandaDadosTreinoServidor();
			}
		});
	}
	
	private void SalvaValoresTemporarios()
	{
		UtilView util = new UtilView();
		Log.d("MinhaTag", "ListView: " + listaDeExercicios.getChildCount() + " elementos | Lista com os exercicios do usuario: " + itemExe.size() + " elementos | Contagem no adapter: " + listaDeExercicios.getAdapter().getCount());
		for(int i = 0; i < itemExe.size(); i++)
		{
			View v = util.getViewPelaPosicao(i, listaDeExercicios);
			
			Spinner tipo = (Spinner) v.findViewById(R.id.spinnerTipoExercicio);
			Spinner exercicio = (Spinner) v.findViewById(R.id.spinnerExercicio);
			
			EditText series = (EditText) v.findViewById(R.id.edtxtSeries);
			EditText repeticoes = (EditText) v.findViewById(R.id.edtxtRepeticao);
			EditText descanso = (EditText) v.findViewById(R.id.edtxtDescanso);
			
			itemExe.get(i).setTipo(tipo.getSelectedItem().toString());
			Log.i("MinhaTag", i + " Spinner: " + exercicio.getSelectedItem().toString());
			itemExe.get(i).setExercicio(exercicio.getSelectedItem().toString());
			
			itemExe.get(i).setSeries(Integer.parseInt(series.getText().toString()));
			itemExe.get(i).setRepeticoes(Integer.parseInt(repeticoes.getText().toString()));
			itemExe.get(i).setDescanso(Integer.parseInt(descanso.getText().toString()));
		}
	}
	
	private void MandaDadosTreinoServidor()
	{
		SalvaValoresTemporarios();
		
		Usuario usuario = new Usuario();
		
		JSONArray json = new JSONArray();
		
		SharedPreferences info = getActivity().getSharedPreferences("Usuario", getActivity().MODE_PRIVATE);
		
		try 
		{
			usuario.recuperaUsuarioJSON(new JSONObject(info.getString("usuario", "Deu errado no mandatreinoservidor")));
		} catch (JSONException e1) {
			e1.printStackTrace();
			Log.e("MinhaTag", "Erro ao recuperar o usuario de um JSON na hora de mandar um treino pro servidor");
		}
        
        //int idUsuario = info.getInt("id_usuario", -1);
		
        for(int i = 0; i < itemExe.size() /*&& idUsuario != -1*/; i++)
		{
			JSONObject j = new JSONObject();
			
			try {
				j.put("fk_usuario", usuario.getID());
				j.put("nomeExercicio", itemExe.get(i).getExercicio());
				j.put("tipo", 1);
				j.put("series", itemExe.get(i).getSeries());
				j.put("repeticoes", itemExe.get(i).getRepeticoes());
				j.put("descanso", itemExe.get(i).getDescanso());
				j.put("dia", dia);
				
				json.put(i, j);
			} catch (JSONException e) {
				Log.e("MinhaTag", "Erro no JSON na hora de enviar treino");
				e.printStackTrace();
			}
		}
        
        
        if(json.length() > 0)
        {
        	Log.d("MinhaTag", "ArrayJSON gerado no clique do botao OK: " + json.toString());
        	EnviaTreinoDiaDaSemana task = new EnviaTreinoDiaDaSemana(this, json);
        	task.execute(json);
        }
	}
}
