package fragmentos;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.tcc.R;
import com.example.tcc.R.id;
import com.example.tcc.R.layout;

import util.UtilView;

import modelos.ItemExercicio;
import modelos.Usuario;
import adapters.ListaItemExerciciosAdapter;
import adapters.ListaItemExerciciosFinalAdapter;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class FragmentoTreino extends Fragment 
{
	private String dia;
	
	private ArrayList<ItemExercicio> itemExe = new ArrayList<ItemExercicio>();
	
	ListView listaDeExercicios;
	
	public FragmentoTreino(String dia)
	{
		this.dia = dia;
	}
	
	@Override
	public View onCreateView(LayoutInflater i, ViewGroup C, Bundle dadoSalvo)
	{
		SharedPreferences info = getActivity().getSharedPreferences("Usuario", Activity.MODE_PRIVATE);
			
		Usuario usuario = new Usuario();
			
		try 
		{
			usuario.recuperaUsuarioJSON(new JSONObject(info.getString("usuario", "")));
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao recuperar o JSON do usuario na tela de novo treino dia: " + dia);
		}
			
		UtilView util = new UtilView();
		
		int diaNumero = util.numeroDiaDaSemana(dia);
			
		for(int x = 0; x < usuario.getTreino(diaNumero).getNumeroDeExerciciosNotreino(); x++)
		{
			ItemExercicio exer = new ItemExercicio();
				
			exer = usuario.getTreino(diaNumero).getItemExercicioDaLista(x);
				
			itemExe.add(exer);
		}
		
		View v = i.inflate(R.layout.fragmento_treino, C, false);
		
		listaDeExercicios = (ListView) v.findViewById(R.id.listViewExerciciosFinais);
		
		ArrayAdapter<ItemExercicio> adapter = new ListaItemExerciciosFinalAdapter(this, R.layout.final_item_exercicio, itemExe);
		
		listaDeExercicios.setAdapter(adapter);
		
		return v;
	}
}
