package telas;

import java.util.ArrayList;

import com.example.tcc.R;

import adapters.AdapterSpinnerActionBarItem;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

public class TelaBasePerfil extends ActionBarActivity 
{
	ActionBar ab;
	ArrayList<SpinnerActionBarItem> listaItens;
	AdapterSpinnerActionBarItem meuAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		InicializaActivity();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tela_perfil_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		if (id == R.id.action_novo_projeto) 
		{
			if(this.getClass() != TelaNovoProjeto2.class)
			{
				startActivity(new Intent(getApplicationContext(), TelaNovoProjeto2.class));
			}
		}
		else if (id == R.id.action_evolucao) 
		{
		}
		else if (id == R.id.action_checagem) 
		{
			if(this.getClass() != TelaChecagem.class)
			{
				startActivity(new Intent(getApplicationContext(), TelaChecagem.class));
			}
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	protected void InicializaActivity()
	{
		/*ab = getActionBar();
		ab.setDisplayShowTitleEnabled(false);
        ab.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        
		listaItens = new ArrayList<SpinnerActionBarItem>();
		listaItens.add(new SpinnerActionBarItem("PERFIL", R.drawable.ic_tela_perfil));
		listaItens.add(new SpinnerActionBarItem("TREINOS", R.drawable.ic_tela_treinos));
		listaItens.add(new SpinnerActionBarItem("ALIMENTAÇÃO", R.drawable.ic_tela_alimentacao));
		listaItens.add(new SpinnerActionBarItem("GRUPOS", R.drawable.ic_action_group));
		listaItens.add(new SpinnerActionBarItem("INFORMAÇÃO", R.drawable.ic_action_about));
		
		meuAdapter = new AdapterSpinnerActionBarItem(getApplicationContext(), listaItens);
		
		ab.setListNavigationCallbacks(meuAdapter, this);*/
		
		ab = getActionBar();
        
        ab.setDisplayShowTitleEnabled(false);
        ab.setDisplayShowHomeEnabled(false);
        
        LayoutInflater mInflater = LayoutInflater.from(this);
        final View v = mInflater.inflate(R.layout.actionbar_customizada, null);
        
        ImageView img1 = (ImageView) v.findViewById(R.id.logoCustomizada1);
        ImageView img2 = (ImageView) v.findViewById(R.id.logoCustomizada2);
        
        img1.setImageResource(R.drawable.ic_action_person);
        img2.setImageResource(R.drawable.ic_action_person);
        
        Spinner sp = (Spinner) v.findViewById(R.id.spinneracbr);
        
        //ab.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        
        listaItens = new ArrayList<SpinnerActionBarItem>();
		listaItens.add(new SpinnerActionBarItem("PERFIL", R.drawable.ic_action_person));
		listaItens.add(new SpinnerActionBarItem("TREINOS", R.drawable.ic_tela_treino2));
		listaItens.add(new SpinnerActionBarItem("ALIMENTAÇÃO", R.drawable.ic_tela_alimentacao3));
		listaItens.add(new SpinnerActionBarItem("GRUPOS", R.drawable.ic_action_group));
		listaItens.add(new SpinnerActionBarItem("INFORMAÇÃO", R.drawable.ic_action_about));
        
		meuAdapter = new AdapterSpinnerActionBarItem(getApplicationContext(), listaItens);
        sp.setAdapter(meuAdapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adp, View v1,	int posicao, long id) 
			{
				if(posicao == 1)
				{
					startActivity(new Intent(getApplicationContext(), TelaTreino.class));
				}
				else if(posicao == 2)
				{
					startActivity(new Intent(getApplicationContext(), TelaDieta.class));
				}
				else if(posicao == 3)
				{
					startActivity(new Intent(getApplicationContext(), TelaGrupo.class));
				}
				else if(posicao == 4)
				{
					ImageView logo1 = (ImageView)v.findViewById(R.id.logoCustomizada1);
					ImageView logo2 = (ImageView)v.findViewById(R.id.logoCustomizada2);
					logo1.setImageResource(R.drawable.ic_action_about);
					logo2.setImageResource(R.drawable.ic_action_about);
					//startActivity(new Intent(getApplicationContext(), SegundaActivity.class));
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) 
			{
				
			}
		});
        
        ab.setCustomView(v);
        ab.setSplitBackgroundDrawable(new ColorDrawable(Color.parseColor("#21007F")));
        ab.setDisplayShowCustomEnabled(true);
        
	}
}
