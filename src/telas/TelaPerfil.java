package telas;

import com.example.tcc.R;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class TelaPerfil extends Activity 
{	
	ImageView imgNvProjeto; // imagem de novo projeto
	ImageView imgAntesDepois; // imagem para antes e depois do usuario
	ImageView imgChecagem; // imagem para o usuario fazer a checagem diaria
	ImageView imgPerfilUsuario; // a imagem de perfil do usuario
	
	TextView nomeUsuario;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_perfil);
        
        SharedPreferences info = getSharedPreferences("Usuario", MODE_PRIVATE);
        
        String nome = info.getString("nome", "Nao encontrou nome");
        String sobrenome = info.getString("sobrenome", "Nao encontrou sobrenomenome");
        String pathFoto = info.getString("pathFoto", "sem foto");
        
        nomeUsuario = (TextView) findViewById(R.id.txtNomeUsuario);
        
        nomeUsuario.setText(nome + " " + sobrenome);
        // encontra e referencia os objetos
        imgNvProjeto = (ImageView) findViewById(R.id.ImagemNovoProjeto);
        imgAntesDepois = (ImageView) findViewById(R.id.ImagemAntesDepois);
        imgChecagem = (ImageView) findViewById(R.id.ImagemChecagem);
        imgPerfilUsuario = (ImageView) findViewById(R.id.imagemPerfil);
        
        String servidor = getString(R.string.servidorAtual); 
        Picasso.with(getApplicationContext()).load("http://" + servidor + "/TCC/" + pathFoto).into(imgPerfilUsuario);
        
        setEventosElementosTela();
    }
    
    private void setEventosElementosTela()
    {
    	// Configura o evento de clique das imagens
    	imgNvProjeto.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(), TelaNovoProjeto.class));
			}
		});
    	
		imgAntesDepois.setOnClickListener(new View.OnClickListener() {
					
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(), TelaAntesDepois.class));
			}
		});
		
		imgChecagem.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(), TelaChecagem.class));
			}
		});
    }
}
