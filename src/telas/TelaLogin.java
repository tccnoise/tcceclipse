package telas;

import org.json.JSONException;
import org.json.JSONObject;

import modelos.Treino;
import modelos.Usuario;
import webservice.CarregaInformacao;
import webservice.ValidaUsuario;

import com.example.tcc.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TelaLogin extends Activity 
{
	//EditText email;
	//EditText senha;
	Button btnEntrar;
	Button btnNovaConta;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_login);
		
		SharedPreferences info = getSharedPreferences("Usuario", MODE_PRIVATE);
		Editor edit = info.edit();
		
		edit.clear();
		edit.commit();
		
		final EditText email = (EditText) findViewById(R.id.loginEmail);
		final EditText senha = (EditText) findViewById(R.id.loginSenha);
		Button btnEntrar = (Button) findViewById(R.id.loginBotao);
		Button btnNovaConta = (Button) findViewById(R.id.loginBotaoNovaConta);
		
		final Activity a = this;
		
		btnEntrar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				Log.d("Valores", "email: " + email.getText().toString() + " senha: " + senha.getText().toString());
				
				JSONObject json = new JSONObject();
				
				try 
				{
					json.put("email", email.getText().toString().trim());
					json.put("senha", senha.getText().toString().trim());
				} 
				catch (JSONException e) 
				{
					e.printStackTrace();
				}
				
				ValidaUsuario x = new ValidaUsuario(a, json, "dadosUsuario");
				CarregaInformacao p = new CarregaInformacao(a);
				x.execute();
				p.execute();
			}
		});
		
		btnNovaConta.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				a.startActivity(new Intent(a, TelaNovoUsuario.class));
			}
		});
	}
}
