package telas;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import util.UtilView;

import modelos.Alimento;

import com.example.tcc.R;

import adapters.AlimentosAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

public class TelaNovosAlimentos extends TelaBasePerfil 
{	
	ListView listaAlimentos;
	
	ArrayList<Alimento> alimentosSelecionados = new ArrayList<Alimento>();
	String identificador;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_selecao_alimentos);
		
		Bundle extras = getIntent().getExtras();
		identificador = "";
		
		if(extras == null)
		{
			Log.e("MinhaTag", "Sem nenhum valor na tela de novos alimentos");
		}
		else
		{
			identificador = extras.getString("identificador");
		}
		
		listaAlimentos = (ListView) findViewById(R.id.listViewListaAlimentos);
		
		Button selecionados = (Button) findViewById(R.id.btn_selecaoAlimentos);
		
		ArrayList<Alimento> al = new ArrayList<Alimento>();
		
		SharedPreferences c;
		
		c = getSharedPreferences("Alimentos", MODE_PRIVATE);
		
		String alimentosString = c.getString("listaAlimentos", "N�o encontrou nenhum alimento");
		
		try 
		{
			JSONArray alimentosJSON = new JSONArray(alimentosString);
			
			int i = 0;

			while(i < alimentosJSON.length())
			{
				JSONObject aJson = alimentosJSON.getJSONObject(i);
				Alimento a = new Alimento();
				
				a.recuperaAlimentoJSON(aJson);
				al.add(a);
				
				i++;
			}
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Erro na TelaNovosAlimentos ao recuperar o jsonarray");
		}
		
		listaAlimentos.setAdapter(new AlimentosAdapter(this, R.layout.alimentos_escolha, al));
		
		selecionados.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				SalvaAlimentosSelecionados();
				
				//Intent in = new Intent(getApplicationContext(), TelaNovaDieta2.class);
				//in.putExtra("ListaAlimenntos", alimentosSelecionados);
				
				startActivity(new Intent(getApplicationContext(), TelaNovaDieta2.class));
			}
		});
	}
	
	private void SalvaAlimentosSelecionados()
	{
		UtilView util = new UtilView();
		
		
		for(int i = 0; i < listaAlimentos.getCount(); i ++)
		{
			View v = util.getViewPelaPosicao(i, listaAlimentos);
			//View v = listaAlimentos.getChildAt(i);
			
			CheckBox cb =  (CheckBox) v.findViewById(R.id.cb_alimento);
			
			if(cb.isChecked())
			{
				Alimento a = new Alimento();
				
				a = (Alimento) listaAlimentos.getAdapter().getItem(i);
				alimentosSelecionados.add(a);
			}
		}
		
		JSONArray ja = new JSONArray();
		
		for(int i = 0; i < alimentosSelecionados.size(); i++)
		{
			ja.put(alimentosSelecionados.get(i).toJSON());
		}
		
		SharedPreferences c = getSharedPreferences("Usuario", MODE_PRIVATE);
		Editor edit = c.edit();
		
		edit.putString(identificador, ja.toString());
		
		edit.commit();
		
		Log.d("MinhaTag", c.getString(identificador, "nao acho nada na hora de salvar os alimentos selecionados com checkBox"));
	}
}
