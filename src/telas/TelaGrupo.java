package telas;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.tcc.R;
import com.example.tcc.TelaGrupoInformacoes;
import com.example.tcc.R.id;
import com.example.tcc.R.layout;


import modelos.Grupo;
import modelos.Usuario;

import adapters.ListaGruposAdapter;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TelaGrupo extends TelaBaseGrupo
{	
	ListView listaDeGrupos;
	TextView mensagemTelaDeGrupos;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_grupo);
		
		mensagemTelaDeGrupos = (TextView) findViewById(R.id.txtMensagemTelaDeGrupo);
		
		listaDeGrupos = (ListView) findViewById(R.id.listViewGruposDoUsuario);
		
		Usuario usuario = new Usuario();
		
		SharedPreferences info = getSharedPreferences("Usuario", MODE_PRIVATE);
		
		try 
		{
			usuario.recuperaUsuarioJSON(new JSONObject(info.getString("usuario", "Nao foi possivel recuperar os grupos do usuario")));
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao tentar recuperar um usuario de um json na tela de grupo");
		}
		
		ArrayAdapter<Grupo> adapter = new ListaGruposAdapter(this, R.layout.item_grupo, usuario.getGrupos());
		
		listaDeGrupos.setAdapter(adapter);
		
		if(listaDeGrupos.getAdapter().getCount() > 0)
		{
			mensagemTelaDeGrupos.setVisibility(View.GONE);
		}
		
		final Activity aux = this;
		
		listaDeGrupos.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View v, int posicao,long id) 
			{
				Grupo g = (Grupo) adapter.getItemAtPosition(posicao);
				
				Intent i = new Intent(aux, TelaGrupoInformacoes.class);
				i.putExtra("grupo", g.toJSON().toString());
				aux.startActivity(i);
			}
		});
	}
}
