package telas;

import java.util.ArrayList;

import modelos.Grupo;
import modelos.Usuario;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import webservice.PesquisaExercicioWS;
import webservice.PesquisaGrupoWS;
import webservice.RequisitaEntradaGrupo;

import com.example.tcc.R;
import com.example.tcc.R.layout;

import adapters.ListaGruposAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PesquisaGrupos extends TelaBaseGrupo {
	
	ListView resultadoPesquisa;
	EditText filtroPesquisa;
	Button btnPesquisa;
	
	PesquisaGrupos aux;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pesquisa_grupos);
		
		filtroPesquisa = (EditText) findViewById(R.id.edtxtPesquisaGrupos);
		btnPesquisa = (Button) findViewById(R.id.btnPesquisaGrupos);
		
		resultadoPesquisa = (ListView) findViewById(R.id.listViewResultadoPesquisaGrupos);
		
		
		aux = this;
		
		btnPesquisa.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				PesquisaGrupoWS task = new PesquisaGrupoWS(aux, filtroPesquisa.getText().toString());
				task.execute();
			}
		});
	}
	
	public void retornaPesquisa(ArrayList<Grupo> grupos)
	{
		resultadoPesquisa.setAdapter(new ListaGruposAdapter(this,R.layout.item_grupo, grupos));
		resultadoPesquisa.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View v, int posicao, long id) 
			{
				final Grupo g = (Grupo) adapter.getItemAtPosition(posicao);
				
				new AlertDialog.Builder(aux)
				.setTitle("Requisitar entrada em grupo")
				.setMessage("Voc� realmente quer entrar no grupo " + g.getNomeDoGrupo())
				.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface d, int id) {
						Usuario usuario = new Usuario();
						
						SharedPreferences info = aux.getSharedPreferences("Usuario", aux.MODE_PRIVATE);
						
						try 
						{
							usuario.recuperaUsuarioJSON(new JSONObject(info.getString("usuario", "Nao foi possivel recuperra o json do usuario nas preferencias")));
						} 
						catch (JSONException e) 
						{
							e.printStackTrace();
						}
						
						RequisitaEntradaGrupo task = new RequisitaEntradaGrupo(aux, usuario, g.getId());
						task.execute();
					}
				})
				.setNegativeButton("N�o", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
					}
				})
				.show();
			}
		});
	}
	
	public void naoRetornaPesquisa()
	{
		resultadoPesquisa.setAdapter(null);
	}
}
