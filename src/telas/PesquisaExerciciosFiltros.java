package telas;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import webservice.PesquisaExercicioWS;

import com.example.tcc.R;
import com.example.tcc.R.layout;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

public class PesquisaExerciciosFiltros extends TelaBaseTreino {
	
	ListView resultadoPesquisa;
	Spinner filtroPesquisa;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pesquisa_exercicios_filtros);
		
		filtroPesquisa = (Spinner) findViewById(R.id.spinnerMusculosFiltros);
		
		SharedPreferences info = getSharedPreferences("Musculos", MODE_PRIVATE);
		
		JSONArray jMusculos = null;
		try 
		{
			jMusculos = new JSONArray(info.getString("listaMusculos", "Nao encontrou musculos"));
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao tentar recuperar um jsonarray: " + info.getString("musculos", "Nao encontrou musculos"));
		}
		
		ArrayList<String> musculos = new ArrayList<String>();
		musculos.add("");
		
		for(int i = 0; i < jMusculos.length(); i++)
		{
			try 
			{
				musculos.add(jMusculos.getJSONObject(i).getString("nomeMusculo"));
			} 
			catch (JSONException e) 
			{
				e.printStackTrace();
			}
		}
		
		filtroPesquisa.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, musculos));
		
		resultadoPesquisa = (ListView) findViewById(R.id.listViewResultadoPesquisaExercicios);
		
		
		final PesquisaExerciciosFiltros aux = this;
		filtroPesquisa.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adapter, View v, int posicao, long id) 
			{
				if(posicao != 0)
				{
					PesquisaExercicioWS task = new PesquisaExercicioWS(aux, filtroPesquisa.getSelectedItem().toString());
					task.execute();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapter) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void retornaPesquisa(ArrayList<String> exercicios)
	{
		resultadoPesquisa.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, exercicios));
	}
	
	public void naoRetornaPesquisa()
	{
		resultadoPesquisa.setAdapter(null);
	}
}
