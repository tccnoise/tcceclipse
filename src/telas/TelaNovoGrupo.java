package telas;

import org.json.JSONException;
import org.json.JSONObject;

import modelos.Treino;
import modelos.Usuario;
import webservice.CarregaInformacao;
import webservice.CriaNovoGrupo;
import webservice.ValidaUsuario;

import com.example.tcc.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TelaNovoGrupo extends TelaBaseGrupo 
{
	//EditText email;
	//EditText senha;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_novo_grupo);
		
		/*SharedPreferences info = getSharedPreferences("Usuario", MODE_PRIVATE);
		Editor edit = info.edit();
		
		edit.clear();
		edit.commit();*/
		
		final EditText nome = (EditText) findViewById(R.id.edtxtNovoNomeGrupo);
		final EditText desc = (EditText) findViewById(R.id.edtxtNovaDescricao);
		
		Button btnNovoGrupo = (Button) findViewById(R.id.btnNovoGrupo);
		
		final Activity a = this;
		
		btnNovoGrupo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				if(!nome.getText().toString().trim().equals("") && !desc.getText().toString().trim().equals(""))
				{
					CriaNovoGrupo task = new CriaNovoGrupo(a, nome.getText().toString().trim(), desc.getText().toString().trim());
					task.execute();
				}
			}
		});
	}
}
