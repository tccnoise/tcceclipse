package telas;

import java.util.ArrayList;
import java.util.List;

import modelos.ItemExercicio;

import com.example.tcc.R;

import adapters.ListaItemExerciciosAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

public class TelaNovoTreino extends Activity
{
	private ListView listaDeExercicios;
	private Button btnAdiciona;
	private Button btnOk;
	
	private List<ItemExercicio> itemExe = new ArrayList();
	
	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_novo_treino);
        
        listaDeExercicios = (ListView) findViewById(R.id.listaExercicios);
        
        btnAdiciona = (Button) findViewById(R.id.btnAddExer);
        btnOk = (Button) findViewById(R.id.btnOkExer);
        
        montaElementosListView();
    }
	
	private void montaElementosListView()
	{
        itemExe.add(new ItemExercicio());
        
        ArrayAdapter<ItemExercicio> adapter = new ListaItemExerciciosAdapter(this, android.R.layout.simple_list_item_1, itemExe);
        
        listaDeExercicios.setAdapter(adapter);
        
        final Activity aux = this;
        
        btnAdiciona.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				itemExe.add(new ItemExercicio());
				
				ArrayAdapter<ItemExercicio> adapter = new ListaItemExerciciosAdapter(aux, android.R.layout.simple_list_item_1, itemExe);
		        
		        listaDeExercicios.setAdapter(adapter);
			}
		});
	}
}
