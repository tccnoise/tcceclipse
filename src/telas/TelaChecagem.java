package telas;

import com.example.tcc.R;

import adapters.AdapterViewPagerChecagem;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

public class TelaChecagem extends TelaBasePerfil
{
	ViewPager pagina;
	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_pagerview);
        
        pagina = (ViewPager) findViewById(R.id.pagina_pagerview);
		FragmentManager fm = getSupportFragmentManager();
		
		pagina.setAdapter(new AdapterViewPagerChecagem(fm));
    }
}
