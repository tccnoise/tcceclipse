package telas;

import adapters.AdapterViewPagerNovoTreino;
import adapters.AdapterViewPagerTreino;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.example.tcc.R;

public class TelaTreino extends TelaBaseTreino
{
	private ViewPager pagina;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_pagerview);
		
		pagina = (ViewPager) findViewById(R.id.pagina_pagerview);
		FragmentManager fm = getSupportFragmentManager();
		
		pagina.setAdapter(new AdapterViewPagerTreino(fm));
    }
}