package telas;

import org.json.JSONException;
import org.json.JSONObject;

import modelos.Treino;
import modelos.Usuario;
import webservice.CarregaInformacao;
import webservice.ValidaUsuario;

import com.example.tcc.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TelaNovoUsuario extends Activity 
{
	//EditText email;
	//EditText senha;
	Button btnNovaConta;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_novo_usuario);
		
		SharedPreferences info = getSharedPreferences("Usuario", MODE_PRIVATE);
		Editor edit = info.edit();
		
		edit.clear();
		edit.commit();
		
		final EditText email = (EditText) findViewById(R.id.nvUsuarioEmail);
		final EditText senha = (EditText) findViewById(R.id.nvUsuarioSenha);
		final EditText nome = (EditText) findViewById(R.id.novoNome);
		final EditText sobrenome = (EditText) findViewById(R.id.novoSobrenome);
		
		Button btnNovaConta = (Button) findViewById(R.id.nvUsuarioBotaoNovaConta);
		
		final Activity a = this;
		
		btnNovaConta.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				Log.d("Valores", "email: " + email.getText().toString() + " senha: " + senha.getText().toString());
				
				JSONObject json = new JSONObject();
				
				try 
				{
					json.put("email", email.getText().toString().trim());
					json.put("senha", senha.getText().toString().trim());
					json.put("nome", nome.getText().toString().trim());
					json.put("sobrenome", sobrenome.getText().toString().trim());
				} 
				catch (JSONException e) 
				{
					e.printStackTrace();
				}
				
				ValidaUsuario x = new ValidaUsuario(a, json, "dadosNovoUsuario");
				CarregaInformacao p = new CarregaInformacao(a);
				x.execute();
				p.execute();
			}
		});
	}
}
