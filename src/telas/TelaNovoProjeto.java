package telas;

import com.example.tcc.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TelaNovoProjeto extends Activity
{
	LinearLayout layoutMedidas;
	LinearLayout layoutDieta;
	LinearLayout layoutTreino;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_novo_projeto);
        
        layoutMedidas = (LinearLayout) findViewById(R.id.layoutMedidas);
        layoutDieta = (LinearLayout) findViewById(R.id.layoutDieta);
        layoutTreino = (LinearLayout) findViewById(R.id.layoutTreino);
        
        setEventosElementosTela();
    }
	
	private void setEventosElementosTela()
	{
		layoutMedidas.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(), TelaNovasMedidas.class));
			}
		});
        
        layoutDieta.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(), TelaNovaDieta.class));
			}
		});
        
        layoutTreino.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(), TelaNovoTreino.class));
			}
		});
	}

}
