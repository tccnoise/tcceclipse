package telas;

public class SpinnerActionBarItem 
{
	private String titulo;
	private int icon;
	
	public SpinnerActionBarItem(String s, int i)
	{
		icon = i;
		titulo = s;
	}
	
	public int getIcon(){
		return icon;
	}
	
	public String getTitulo(){
		return titulo;
	}
}
