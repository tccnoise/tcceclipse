package telas;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import modelos.Usuario;

import com.example.tcc.R;
import com.squareup.picasso.Picasso;

import adapters.AdapterSpinnerActionBarItem;
import adapters.ListaAtributosAdapter;
import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class TelaPerfil2 extends TelaBasePerfil 
{
	ImageView imgPerfilUsuario; // a imagem de perfil do usuario
	
	TextView nomeUsuario; // nome do usuario
	
	ListView atributosAtuais;
	ListView atributosObjetivo;
	
	
	ArrayList<String> atAtual = new ArrayList<String>();
	//ArrayList<String> atObjetivo = new ArrayList<String>();
	ArrayList<Float> vaAtual = new ArrayList<Float>();
	ArrayList<Float> vaObjetivo = new ArrayList<Float>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.telaperfil2);
		
		SharedPreferences info = getSharedPreferences("Usuario", MODE_PRIVATE);
		
		Usuario usuario = new Usuario();
		
		try 
		{
			usuario.recuperaUsuarioJSON(new JSONObject(info.getString("usuario", "Nao foi encontrado nenhum usuario")));
		} catch (JSONException e) 
		{
			Log.e("MinhaTag", "Erro ao recuperar um usuario de JSON na tela de perfil");
			e.printStackTrace();
		}
		
		imgPerfilUsuario = (ImageView) findViewById(R.id.imagemPerfil);
		nomeUsuario = (TextView) findViewById(R.id.txtNomeUsuario);
		String pathFoto = usuario.getPathImg();
		
		atributosAtuais = (ListView) findViewById(R.id.listAtributosAtuais);
		atributosObjetivo = (ListView) findViewById(R.id.listAtributosObjetivos);
		
		MontaListas(usuario);
		/*atAtual.add("Peso");
		atAtual.add("Bra�o");
		atAtual.add("Panturrilha");
		
		vaAtual.add(15f);
		vaAtual.add(15f);
		vaAtual.add(15f);
		vaObjetivo.add(17f);
		vaObjetivo.add(17f);
		vaObjetivo.add(17f);*/
		
		atributosAtuais.setAdapter(new ListaAtributosAdapter(this, R.layout.item_medidas_perfil, atAtual, vaAtual));
		atributosObjetivo.setAdapter(new ListaAtributosAdapter(this, R.layout.item_medidas_perfil, atAtual, vaObjetivo));
		
		String servidor = getString(R.string.servidorAtual); 
        Picasso.with(getApplicationContext()).load("http://" + servidor + "/TCC/" + pathFoto).into(imgPerfilUsuario);
        
        nomeUsuario.setText(usuario.getNome() + " " + usuario.getSobrenome());
	}
	
	private void MontaListas(Usuario usuario)
	{
		if(usuario.existePeso())
		{
			atAtual.add("Peso");
			vaAtual.add(usuario.getPesoAtual());
			vaObjetivo.add(usuario.getPesoObjetivo());			
		}
		
		if(usuario.existeTorax())
		{
			atAtual.add("Torax");
			vaAtual.add(usuario.getToraxAtual());
			vaObjetivo.add(usuario.getToraxObjetivo());			
		}
		
		if(usuario.existeBraco())
		{
			atAtual.add("Bra�o");
			vaAtual.add(usuario.getBracoAtual());
			vaObjetivo.add(usuario.getBracoObjetivo());			
		}
		
		if(usuario.existeCintura())
		{
			atAtual.add("Cintura");
			vaAtual.add(usuario.getCinturaAtual());
			vaObjetivo.add(usuario.getCinturaObjetivo());			
		}
		
		if(usuario.existeCostas())
		{
			atAtual.add("Costas");
			vaAtual.add(usuario.getCostasAtual());
			vaObjetivo.add(usuario.getCostasObjetivo());			
		}
		
		if(usuario.existeCoxas())
		{
			atAtual.add("Coxas");
			vaAtual.add(usuario.getCoxasAtual());
			vaObjetivo.add(usuario.getCoxasObjetivo());			
		}
		
		if(usuario.existePanturrilha())
		{
			atAtual.add("Panturrilha");
			vaAtual.add(usuario.getPanturrilhaAtual());
			vaObjetivo.add(usuario.getPanturrilhaObjetivo());			
		}
	}
}
