package telas;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.example.tcc.R;

public class TelaNovoProjeto2 extends TelaBasePerfil 
{
	LinearLayout layoutMedidas;
	LinearLayout layoutDieta;
	LinearLayout layoutTreino;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_novo_projeto);
		
		layoutMedidas = (LinearLayout) findViewById(R.id.layoutMedidas);
        layoutDieta = (LinearLayout) findViewById(R.id.layoutDieta);
        layoutTreino = (LinearLayout) findViewById(R.id.layoutTreino);
        
        setEventosElementosTela();
	}
	
	private void setEventosElementosTela()
	{
		layoutMedidas.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				startActivity(new Intent(getApplicationContext(), TelaNovasMedidas.class));
			}
		});
        
        layoutDieta.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				startActivity(new Intent(getApplicationContext(), TelaNovaDieta2.class));
			}
		});
        
        layoutTreino.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				startActivity(new Intent(getApplicationContext(), TelaNovoTreino2.class));
			}
		});
	}
}
