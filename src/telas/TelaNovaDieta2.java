package telas;

import java.util.ArrayList;
import java.util.List;

import adapters.AdapterViewPagerNovaDieta;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.tcc.R;

public class TelaNovaDieta2 extends TelaBasePerfil 
{
	private ViewPager pagina;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tela_pagerview);
		
		pagina = (ViewPager) findViewById(R.id.pagina_pagerview);
		FragmentManager fm = getSupportFragmentManager();
		
		pagina.setAdapter(new AdapterViewPagerNovaDieta(fm));
    }
}