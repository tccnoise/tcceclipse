package telas;

import org.json.JSONException;
import org.json.JSONObject;

import webservice.EnviaNovasMedidas;

import modelos.Usuario;

import com.example.tcc.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TelaNovasMedidas extends TelaBasePerfil
{
	Button btnEnviar;
	
	EditText etPesoAtual;
	EditText etPesoObjetivo;
	
	EditText etToraxAtual;
	EditText etToraxObjetivo;
	
	EditText etBracoAtual;
	EditText etBracoObjetivo;
	
	EditText etCinturaAtual;
	EditText etCinturaObjetivo;
	
	EditText etCostasAtual;
	EditText etCostasObjetivo;
	
	EditText etCoxasAtual;
	EditText etCoxasObjetivo;
	
	EditText etPanturrilhaAtual;
	EditText etPanturrilhaObjetivo;
	
	Usuario usuario;
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tela_novas_medidas);
        
        setElementosDaTela();
        
        final Activity a = this;
        
        btnEnviar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(!TextUtils.isEmpty(etPesoAtual.getText()) && !TextUtils.isEmpty(etPesoObjetivo.getText()))
				{
					float atual = Float.parseFloat(etPesoAtual.getText().toString());
					float objetivo = Float.parseFloat(etPesoObjetivo.getText().toString());
					
					usuario.setPeso(atual, objetivo, true);
				}
				else
				{
					usuario.setPeso(-1, -1, false);
				}
				
				if(!TextUtils.isEmpty(etToraxAtual.getText()) && !TextUtils.isEmpty(etToraxObjetivo.getText()))
				{
					float atual = Float.parseFloat(etToraxAtual.getText().toString());
					float objetivo = Float.parseFloat(etToraxObjetivo.getText().toString());
					
					usuario.setTorax(atual, objetivo, true);
				}
				else
				{
					usuario.setTorax(-1, -1, false);
				}
				
				if(!TextUtils.isEmpty(etBracoAtual.getText()) && !TextUtils.isEmpty(etBracoObjetivo.getText()))
				{
					float atual = Float.parseFloat(etBracoAtual.getText().toString());
					float objetivo = Float.parseFloat(etBracoObjetivo.getText().toString());
					
					usuario.setBraco(atual, objetivo, true);
				}
				else
				{
					usuario.setBraco(-1, -1, false);
				}
				
				if(!TextUtils.isEmpty(etCinturaAtual.getText()) && !TextUtils.isEmpty(etCinturaObjetivo.getText()))
				{
					float atual = Float.parseFloat(etCinturaAtual.getText().toString());
					float objetivo = Float.parseFloat(etCinturaObjetivo.getText().toString());
					
					usuario.setCintura(atual, objetivo, true);
				}
				else
				{
					usuario.setCintura(-1, -1, false);
				}
				
				if(!TextUtils.isEmpty(etCostasAtual.getText()) && !TextUtils.isEmpty(etCostasObjetivo.getText()))
				{
					float atual = Float.parseFloat(etCostasAtual.getText().toString());
					float objetivo = Float.parseFloat(etCostasObjetivo.getText().toString());
					
					usuario.setCostas(atual, objetivo, true);
				}
				else
				{
					usuario.setCostas(-1, -1, false);
				}
				
				if(!TextUtils.isEmpty(etCoxasAtual.getText()) && !TextUtils.isEmpty(etCoxasObjetivo.getText()))
				{
					float atual = Float.parseFloat(etCoxasAtual.getText().toString());
					float objetivo = Float.parseFloat(etCoxasObjetivo.getText().toString());
					
					usuario.setCoxas(atual, objetivo, true);
				}
				else
				{
					usuario.setCoxas(-1, -1, false);
				}
				
				if(!TextUtils.isEmpty(etPanturrilhaAtual.getText()) && !TextUtils.isEmpty(etPanturrilhaObjetivo.getText()))
				{
					float atual = Float.parseFloat(etPanturrilhaAtual.getText().toString());
					float objetivo = Float.parseFloat(etPanturrilhaObjetivo.getText().toString());
					
					usuario.setPanturrilha(atual, objetivo, true);
				}
				else
				{
					usuario.setPanturrilha(-1, -1, false);
				}
				
				EnviaNovasMedidas task = new EnviaNovasMedidas(usuario, a);
				task.execute();
			}
		});
    }
	
	private void setElementosDaTela()
	{
		SharedPreferences info = getSharedPreferences("Usuario", MODE_PRIVATE);
		
		usuario = new Usuario();
		
		try 
		{
			usuario.recuperaUsuarioJSON(new JSONObject(info.getString("usuario", "Nao foi encontrado nenhum usuario")));
		} catch (JSONException e) 
		{
			Log.e("MinhaTag", "Erro ao recuperar um usuario de JSON na tela de novas medidas");
			e.printStackTrace();
		}
		
		btnEnviar = (Button) findViewById(R.id.btnNovasMedidas);
		
		etPesoAtual = (EditText) findViewById(R.id.edtxtPesoAtual);
		etPesoObjetivo = (EditText) findViewById(R.id.edtxtPesoObjetivo);
		
		if(usuario.existePeso())
		{
			etPesoAtual.setText(""+ usuario.getPesoAtual());
			etPesoObjetivo.setText(""+ usuario.getPesoObjetivo());
		}
		
		etToraxAtual = (EditText) findViewById(R.id.edtxtToraxAtual);
		etToraxObjetivo = (EditText) findViewById(R.id.edtxtToraxObjetivo);
		
		if(usuario.existeTorax())
		{
			etToraxAtual.setText(""+ usuario.getToraxAtual());
			etToraxObjetivo.setText(""+ usuario.getToraxObjetivo());
		}
		
		etBracoAtual = (EditText) findViewById(R.id.edtxtBracoAtual);
		etBracoObjetivo = (EditText) findViewById(R.id.edtxtBracoObjetivo);
		
		if(usuario.existeBraco())
		{
			etBracoAtual.setText(""+ usuario.getBracoAtual());
			etBracoObjetivo.setText(""+ usuario.getBracoObjetivo());
		}
		
		etCinturaAtual = (EditText) findViewById(R.id.edtxtCinturaAtual);
		etCinturaObjetivo = (EditText) findViewById(R.id.edtxtCinturaObjetivo);
		
		if(usuario.existeCintura())
		{
			etCinturaAtual.setText(""+ usuario.getCinturaAtual());
			etCinturaObjetivo.setText(""+ usuario.getCinturaObjetivo());
		}
		
		etCostasAtual = (EditText) findViewById(R.id.edtxtCostasAtual);
		etCostasObjetivo = (EditText) findViewById(R.id.edtxtCostasObjetivo);
		
		if(usuario.existeCostas())
		{
			etCostasAtual.setText(""+ usuario.getCostasAtual());
			etCostasObjetivo.setText(""+ usuario.getCostasObjetivo());
		}
		
		etCoxasAtual = (EditText) findViewById(R.id.edtxtCoxasAtual);
		etCoxasObjetivo = (EditText) findViewById(R.id.edtxtCoxasObjetivo);
		
		if(usuario.existeCoxas())
		{
			etCoxasAtual.setText(""+ usuario.getCoxasAtual());
			etCoxasObjetivo.setText(""+ usuario.getCoxasObjetivo());
		}
		
		etPanturrilhaAtual = (EditText) findViewById(R.id.edtxtPanturrilhaAtual);
		etPanturrilhaObjetivo = (EditText) findViewById(R.id.edtxtPanturrilhaObjetivo);
		
		if(usuario.existePanturrilha())
		{
			etPanturrilhaAtual.setText(""+ usuario.getPanturrilhaAtual());
			etPanturrilhaObjetivo.setText(""+ usuario.getPanturrilhaObjetivo());
		}
	}
}
