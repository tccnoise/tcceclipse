package modelos;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class UsuarioGrupo 
{
	String nomeUsuarioGrupo;
	float pontosUsuarioGrupo;
	
	public String getNomeUsuarioGrupo()
	{
		return nomeUsuarioGrupo;
	}
	
	public void setNomeUsuarioGrupo(String n)
	{
		nomeUsuarioGrupo = n;
	}
	
	public float getPontosUsuarioGrupo()
	{
		return pontosUsuarioGrupo;
	}
	
	public void setPontosUsuarioGrupo(float p)
	{
		pontosUsuarioGrupo = p;
	}
	
	public JSONObject toJSON()
	{
		JSONObject json = new JSONObject();
		
		try 
		{
			json.put("nomeUsuario", nomeUsuarioGrupo);
			json.put("pontosUsuario", pontosUsuarioGrupo);
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao tentar transformar um UsuarioGrupo em JSON");
		}
		
		return json;
	}
	
	public void recuperaUsuarioGrupoJSON(JSONObject json)
	{
		try 
		{
			nomeUsuarioGrupo = json.getString("nomeUsuario");
			pontosUsuarioGrupo = (float)json.getDouble("pontosUsuario");
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao tentar recuperar um UsuarioGrupo de um JSON");
		}
	}
}
