package modelos;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Treino 
{
	ArrayList<ItemExercicio> Exercicios = new ArrayList<ItemExercicio>();
	
	public Treino()
	{
		/*Exercicios.add(new ItemExercicio());
		ItemExercicio x  = new ItemExercicio();
		x.setExercicio("Douglas");
		Exercicios.add(x);*/
	}
	
	public void AdicionaItemExercicioNaLista(String nomeExercicio, String tipoExercicio, int se, int re, int des)
	{
		ItemExercicio item = new ItemExercicio();
		
		item.setExercicio(nomeExercicio);
		item.setTipo(tipoExercicio);
		item.setSeries(se);
		item.setRepeticoes(re);
		item.setDescanso(des);
		
		Exercicios.add(item);
	}
	
	public ItemExercicio getItemExercicioDaLista(int posicao)
	{
		return Exercicios.get(posicao);
	}
	
	public int getNumeroDeExerciciosNotreino()
	{
		return Exercicios.size();
	}
	
	public JSONArray toJSON()
	{
		JSONArray jsonExercicios = new JSONArray();
		
		for(int i = 0; i < Exercicios.size(); i++)
		{
			jsonExercicios.put(Exercicios.get(i).toJSON());
		}
		
		return jsonExercicios;
	}
	
	public void recuperaTreinoJSON(JSONArray j)
	{
		Exercicios.clear();
		
		for(int i = 0; i < j.length(); i++)
		{
			ItemExercicio item = new ItemExercicio();
			
			try
			{
			item.recuperaItemExercicioJSON(j.getJSONObject(i));
			Exercicios.add(item);
			}
			catch(JSONException e)
			{
				Log.e("MinhaTag", "Erro ao recuperar o treino de um json");
			}
		}
		
		/*for(int i = 0; i < Exercicios.size(); i++)
		{
			Log.d("MinhaTag", "Exercicios settado: " + Exercicios.get(i).getExercicio());
		}*/
	}
	
	public void DebugaDados()
	{
		for(int i = 0; i < Exercicios.size(); i++)
		{
			Log.i("Minhatag", "Dados recuperados: " + Exercicios.get(i).getExercicio());
		}
	}
}
