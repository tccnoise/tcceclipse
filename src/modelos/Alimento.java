package modelos;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Alimento 
{
	long id;
	String nome;
	
	int calorias;
	float carboidratos;
	float proteinas;
	float gordurasTotais;
	float gordurasSaturadas;
	float gordurasTrans;
	float fibraAlimentar;
	float sodio;
	
	float porcao;
	
	String tipo;
	
	public Alimento()
	{
		id = -1;
		nome = "Ma�a";
		tipo = "Frutas e derivados";
		
		calorias = 100;
		carboidratos = 15;
		proteinas = 7;
		gordurasTotais = 15;
		gordurasSaturadas = 9;
		gordurasTrans = 6;
		fibraAlimentar = 1;
		sodio = 0.1f;
		
		porcao = 100;
	}
	
	public long getId()
	{
		return id;
	}
	
	public void setId(long i)
	{
		id = i;
	}
	
	public String getNome()
	{
		return nome;
	}
	
	public void setNome(String n)
	{
		nome = n;
	}
	
	public int getCaloria()
	{
		return calorias;
	}
	
	public void setCaloria(int c)
	{
		calorias = c;
	}
	
	public String getTipo()
	{
		return tipo;
	}
	
	public float getCarboidratos()
	{
		return carboidratos;
	}
	
	public void setCarboidratos(float c)
	{
		carboidratos = c;
	}
	
	public float getProteinas()
	{
		return proteinas;
	}
	
	public void setProteinas(float p)
	{
		proteinas = p;
	}
	
	public float getGordurasTotais()
	{
		return gordurasTotais;
	}
	
	public void setGordurasTotais(float g)
	{
		gordurasTotais = g;
	}
	
	public float getSaturadas()
	{
		return gordurasSaturadas;
	}
	
	public void setSaturadas(float g)
	{
		gordurasSaturadas = g;
	}
	
	public float getGordurasTrans()
	{
		return gordurasTrans;
	}
	
	public void setGordurasTrans(float g)
	{
		gordurasTrans = g;
	}
	
	public float getFibras()
	{
		return fibraAlimentar;
	}
	
	public void setFibras(float f)
	{
		fibraAlimentar = f;
	}
	
	public float getSodio()
	{
		return sodio;
	}
	
	public void setSodio(float s)
	{
		sodio = s;
	}
	
	public float getPorcao()
	{
		return porcao;
	}
	
	public void setPorcao(float p)
	{
		porcao = p;
	}
	
	public void setTipo(String t)
	{
		tipo = t;
	}
	
	public JSONObject toJSON()
	{
		JSONObject json = new JSONObject();
		
		try 
		{
			json.put("idAlimento", id);
			json.put("nomeAlimento", nome);
			json.put("calorias", calorias);
			json.put("carboidratos", carboidratos);
			json.put("proteinas", proteinas);
			json.put("gordurasTotais", gordurasTotais);
			json.put("gordurasSaturadas", gordurasSaturadas);
			json.put("gordurasTrans", gordurasTrans);
			json.put("fibraAlimentar", fibraAlimentar);
			json.put("sodio", sodio);
			json.put("porcao", porcao);
			json.put("tipoAlimento", tipo);
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao gerar um json de algum alimento");
		}
		
		return json;
	}
	
	public void recuperaAlimentoJSON(JSONObject json)
	{
		try 
		{
			id = json.getLong("idAlimento");
			nome = json.getString("nomeAlimento");
			calorias = json.getInt("calorias");
			carboidratos = (float)json.getDouble("carboidratos");
			proteinas = (float)json.getDouble("proteinas");
			gordurasTotais = (float)json.getDouble("gordurasTotais");
			gordurasSaturadas = (float)json.getDouble("gordurasSaturadas");
			gordurasTrans = (float)json.getDouble("gordurasTrans");
			fibraAlimentar = (float)json.getDouble("fibraAlimentar");
			sodio = (float)json.getDouble("sodio");
			porcao = (float)json.getDouble("porcao");
			tipo = json.getString("tipoAlimento");
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao recuperar um alimento de um json");
		}
	}
}
