package modelos;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class ItemRefeicao
{
	String nomeRefeicao;
	ArrayList<Alimento> alimentos = new ArrayList<Alimento>();
	String horario;
	
	//int porcao;
	
	public ItemRefeicao()
	{
		nomeRefeicao = "Caf� da Manh�";
		
		alimentos.add(new Alimento());
		horario = "9:00";
		//porcao = 100;
	}
	
	public String getNomeRefeicao()
	{
		return nomeRefeicao;
	}
	
	public void setNomeRefeicao(String nvRefeicao)
	{
		nomeRefeicao = nvRefeicao;
	}
	
	public Alimento getAlimento(int position)
	{
		return alimentos.get(position);
	}
	
	public void setAlimentos(ArrayList<Alimento> a)
	{
		alimentos = a;
	}
	
	public String getHorario()
	{
		return horario;
	}
	
	public void setHorario(String nvHorario)
	{
		horario = nvHorario;
	}
	
	public int getQuantidadeAlimentos()
	{
		return alimentos.size();
	}
	
	/*public int getPorcao()
	{
		return porcao;
	}
	
	public void setPorcao(int nvPorcao)
	{
		porcao = nvPorcao;
	}*/
	
	public JSONObject toJSON()
	{
		JSONObject json = new JSONObject();
		
		try 
		{
			json.put("nomeRefeicao", nomeRefeicao);
			JSONArray alimentosJA = new JSONArray();
			
			for(int i = 0; i < alimentos.size(); i++)
			{
				alimentosJA.put(alimentos.get(i).toJSON());
			}
			
			json.put("alimentos", alimentosJA);
			json.put("horario", horario);
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao converter um itemRefeicao para JSON");
		}
		
		return json;
	}
	
	public void recuperaItemRefeicao(JSONObject json)
	{
		try 
		{
			nomeRefeicao = json.getString("nomeRefeicao");
			
			JSONArray alimentosJA = json.getJSONArray("alimentos");
			
			alimentos.clear();
			
			for(int i = 0; i < alimentosJA.length(); i++)
			{
				Alimento a = new Alimento();
				
				a.recuperaAlimentoJSON(alimentosJA.getJSONObject(i));
				
				alimentos.add(a);
			}
			
			horario = json.getString("horario");
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao tentar recuperar um ItemRefeicao de um JSON");
		}
	}	
}
