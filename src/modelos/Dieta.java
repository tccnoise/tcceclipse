package modelos;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Dieta 
{
	ArrayList<ItemRefeicao> Refeicoes = new ArrayList<ItemRefeicao>();
	
	public Dieta()
	{
		/*Exercicios.add(new ItemExercicio());
		ItemExercicio x  = new ItemExercicio();
		x.setExercicio("Douglas");
		Exercicios.add(x);*/
	}
	
	public void AdicionaItemRefeicaoNaLista(ItemRefeicao ref)
	{
		Refeicoes.add(ref);
	}
	
	public ItemRefeicao getItemRefeicaoDaLista(int posicao)
	{
		return Refeicoes.get(posicao);
	}
	
	public int getNumeroDeRefeicoesNotreino()
	{
		return Refeicoes.size();
	}
	
	public ArrayList<ItemRefeicao> getListaRefeicoes()
	{
		return Refeicoes;
	}
	
	public void setListaRefeicoes(ArrayList<ItemRefeicao> refs)
	{
		Refeicoes = refs;
	}
	
	public JSONArray toJSON()
	{
		JSONArray jsonRefeicoes = new JSONArray();
		
		for(int i = 0; i < Refeicoes.size(); i++)
		{
			jsonRefeicoes.put(Refeicoes.get(i).toJSON());
		}
		
		return jsonRefeicoes;
	}
	
	public void recuperaDietaJSON(JSONArray j)
	{
		Refeicoes.clear();
		
		for(int i = 0; i < j.length(); i++)
		{
			ItemRefeicao item = new ItemRefeicao();
			
			try
			{
				item.recuperaItemRefeicao(j.getJSONObject(i));
				Refeicoes.add(item);
			}
			catch(JSONException e)
			{
				Log.e("MinhaTag", "Erro ao recuperar a Dieta de um json");
			}
		}
		
		/*for(int i = 0; i < Exercicios.size(); i++)
		{
			Log.d("MinhaTag", "Exercicios settado: " + Exercicios.get(i).getExercicio());
		}*/
	}
	
	public void DebugaDados()
	{
		Log.d("MinhaTag", "Quantidade de ref: " + Refeicoes.size());
		for(int i = 0; i < Refeicoes.size(); i++)
		{
			Log.i("MinhaTag", "Dados recuperados: " + Refeicoes.get(i).getNomeRefeicao());
		}
	}
}
