package modelos;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Usuario 
{
	int id;
	String nome;
	String sobrenome;
	String pathImgPerfil;
	String email;
	
	float pontos;
	float bonus;
	
	String dia;
	String mes;
	String ano;
	
	float pesoAtual;
	float pesoObjetivo;
	boolean peso;
	
	float toraxAtual;
	float toraxObjetivo;
	boolean torax;
	
	float bracoAtual;
	float bracoObjetivo;
	boolean braco;
	
	float cinturaAtual;
	float cinturaObjetivo;
	boolean cintura;
	
	float costasAtual;
	float costasObjetivo;
	boolean costas;
	
	float coxasAtual;
	float coxasObjetivo;
	boolean coxas;
	
	float panturrilhaAtual;
	float panturrilhaObjetivo;
	boolean panturrilha;
	
	ArrayList<Treino> treinos = new ArrayList<Treino>();
	ArrayList<Dieta> dietas = new ArrayList<Dieta>();
	ArrayList<Grupo> grupos = new ArrayList<Grupo>();
	
	public Usuario()
	{
		id = -1;
		nome = "";
		sobrenome = "";
		pathImgPerfil = "";
		email = "";
		
		pontos = 0;
		bonus = 0;
		
		dia = "01";
		mes = "01";
		ano = "0000";
		
		peso = false;
		pesoAtual = -1;
		pesoObjetivo = -1;
		
		torax = false;
		toraxAtual = -1;
		toraxObjetivo = -1;
		
		braco = false;
		bracoAtual = -1;
		bracoObjetivo = -1;
		
		cintura = false;
		cinturaAtual = -1;
		cinturaObjetivo = -1;
		
		costas = false;
		costasAtual = -1;
		costasObjetivo = -1;
		
		coxas = false;
		coxasAtual = -1;
		coxasObjetivo = -1;
		
		panturrilha = false;
		panturrilhaAtual = -1;
		panturrilhaObjetivo = -1;
		
		
		treinos.add(new Treino());
		treinos.add(new Treino());
		treinos.add(new Treino());
		treinos.add(new Treino());
		treinos.add(new Treino());
		treinos.add(new Treino());
		treinos.add(new Treino());
		
		dietas.add(new Dieta());
		dietas.add(new Dieta());
		dietas.add(new Dieta());
		dietas.add(new Dieta());
		dietas.add(new Dieta());
		dietas.add(new Dieta());
		dietas.add(new Dieta());
	}
	
	public Usuario(int ID, String Nome, String Sobrenome, String img, String Email)
	{
		id = ID;
		nome = Nome;
		sobrenome = Sobrenome;
		pathImgPerfil = img;
		email = Email;
		
		pontos = 0;
		bonus = 0;
		
		dia = "01";
		mes = "01";
		ano = "0000";
		
		peso = false;
		pesoAtual = -1;
		pesoObjetivo = -1;
		
		torax = false;
		toraxAtual = -1;
		toraxObjetivo = -1;
		
		braco = false;
		bracoAtual = -1;
		bracoObjetivo = -1;
		
		cintura = false;
		cinturaAtual = -1;
		cinturaObjetivo = -1;
		
		costas = false;
		costasAtual = -1;
		costasObjetivo = -1;
		
		coxas = false;
		coxasAtual = -1;
		coxasObjetivo = -1;
		
		panturrilha = false;
		panturrilhaAtual = -1;
		panturrilhaObjetivo = -1;
		
		treinos.add(new Treino());
		treinos.add(new Treino());
		treinos.add(new Treino());
		treinos.add(new Treino());
		treinos.add(new Treino());
		treinos.add(new Treino());
		treinos.add(new Treino());
		
		dietas.add(new Dieta());
		dietas.add(new Dieta());
		dietas.add(new Dieta());
		dietas.add(new Dieta());
		dietas.add(new Dieta());
		dietas.add(new Dieta());
		dietas.add(new Dieta());
	}
	
	public Treino getTreino(int dia)
	{
		return treinos.get(dia);
	}
	
	public void setTreino(Treino t, int dia)
	{
		treinos.set(dia, t);
	}
	
	public Dieta getDieta(int dia)
	{
		return dietas.get(dia);
	}
	
	public void setDieta(Dieta d, int dia)
	{
		dietas.set(dia, d);
	}
	
	public ArrayList<Grupo> getGrupos()
	{
		return grupos;
	}
	
	public void setGrupos(ArrayList<Grupo> g)
	{
		grupos = g;
	}
	
	public int getID()
	{
		return id;
	}
	
	public String getNome()
	{
		return nome;
	}
	
	public String getSobrenome()
	{
		return sobrenome;
	}
	
	public String getPathImg()
	{
		return pathImgPerfil;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	public void setPontos(float p)
	{
		pontos = p;
	}
	
	public float getPontos()
	{
		return pontos;
	}
	
	public void setBonus(float b)
	{
		bonus = b;
	}
	
	public float getBonus()
	{
		return bonus;
	}
	
	public void setDia(String d)
	{
		dia = d;
	}
	
	public String getDia()
	{
		return dia;
	}
	
	public void setMes(String m)
	{
		mes = m;
	}
	
	public String getMes()
	{
		return mes;
	}
	
	public void setAno(String a)
	{
		ano = a;
	}
	
	public String getAno()
	{
		return ano;
	}
	
	public float getPesoAtual()
	{
		return pesoAtual;
	}
	
	public float getPesoObjetivo()
	{
		return pesoObjetivo;
	}
	
	public boolean existePeso()
	{
		return peso;
	}
	
	public void setPeso(float pA, float pO, boolean p)
	{
		pesoAtual = pA;
		pesoObjetivo = pO;
		
		peso = p;
	}
	
	public float getToraxAtual()
	{
		return toraxAtual;
	}
	
	public float getToraxObjetivo()
	{
		return toraxObjetivo;
	}
	
	public boolean existeTorax()
	{
		return torax;
	}
	
	public void setTorax(float tA, float tO, boolean t)
	{
		toraxAtual = tA;
		toraxObjetivo = tO;
		
		torax = t;
	}
	
	public float getBracoAtual()
	{
		return bracoAtual;
	}
	
	public float getBracoObjetivo()
	{
		return bracoObjetivo;
	}
	
	public boolean existeBraco()
	{
		return braco;
	}
	
	public void setBraco(float bA, float bO, boolean b)
	{
		bracoAtual = bA;
		bracoObjetivo = bO;
		
		braco = b;
	}
	
	public float getCinturaAtual()
	{
		return cinturaAtual;
	}
	
	public float getCinturaObjetivo()
	{
		return cinturaObjetivo;
	}
	
	public boolean existeCintura()
	{
		return cintura;
	}
	
	public void setCintura(float cA, float cO, boolean c)
	{
		cinturaAtual = cA;
		cinturaObjetivo = cO;
		
		cintura = c;
	}
	
	public float getCostasAtual()
	{
		return costasAtual;
	}
	
	public float getCostasObjetivo()
	{
		return costasObjetivo;
	}
	
	public boolean existeCostas()
	{
		return costas;
	}
	
	public void setCostas(float cA, float cO, boolean c)
	{
		costasAtual = cA;
		costasObjetivo = cO;
		
		costas = c;
	}
	
	public float getCoxasAtual()
	{
		return coxasAtual;
	}
	
	public float getCoxasObjetivo()
	{
		return coxasObjetivo;
	}
	
	public boolean existeCoxas()
	{
		return coxas;
	}
	
	public void setCoxas(float cA, float cO, boolean c)
	{
		coxasAtual = cA;
		coxasObjetivo = cO;
		
		coxas = c;
	}
	
	public float getPanturrilhaAtual()
	{
		return panturrilhaAtual;
	}
	
	public float getPanturrilhaObjetivo()
	{
		return panturrilhaObjetivo;
	}
	
	public boolean existePanturrilha()
	{
		return panturrilha;
	}
	
	public void setPanturrilha(float pA, float pO, boolean p)
	{
		panturrilhaAtual = pA;
		panturrilhaObjetivo = pO;
		
		panturrilha = p;
	}
	
	public JSONObject toJSON()
	{
		JSONObject json = new JSONObject();
		
		try 
		{
			json.put("id", id);
			json.put("nome", nome);
			json.put("sobrenome", sobrenome);
			json.put("imagem", pathImgPerfil);
			json.put("email", email);
			
			json.put("pontos", pontos);
			json.put("bonus", bonus);
			
			json.put("dia", dia);
			json.put("mes", mes);
			json.put("ano", ano);
			
			JSONArray treinoJson = new JSONArray();
			
			for(int i = 0; i < treinos.size(); i++)
			{
				treinoJson.put(treinos.get(i).toJSON());
			}
			
			//Log.d("MinhaTag", "JSON dos treinos: " + treinoJson.toString());
			
			json.put("treinos", treinoJson);
			
			JSONArray dietaJson = new JSONArray();
			
			for(int i = 0; i < dietas.size(); i++)
			{
				dietaJson.put(dietas.get(i).toJSON());
			}
			
			json.put("dietas", dietaJson);
			
			JSONArray gruposJson = new JSONArray();
			
			for(int i = 0; i < grupos.size(); i++)
			{
				gruposJson.put(grupos.get(i).toJSON());
			}
			
			Log.i("MinhaTag", gruposJson.toString());
			json.put("grupos", gruposJson);
			
			if(peso)
			{
				json.put("pesoAtual", pesoAtual);
				json.put("pesoObjetivo", pesoObjetivo);
				json.put("peso", true);
			}
			else
			{
				json.put("pesoAtual", -1);
				json.put("pesoObjetivo", -1);
				json.put("peso", false);
			}
			
			if(torax)
			{
				json.put("toraxAtual", toraxAtual);
				json.put("toraxObjetivo", toraxObjetivo);
				json.put("torax", true);
			}
			else
			{
				json.put("toraxAtual", -1);
				json.put("toraxObjetivo", -1);
				json.put("torax", false);
			}
			
			if(braco)
			{
				json.put("bracoAtual", bracoAtual);
				json.put("bracoObjetivo", bracoObjetivo);
				json.put("braco", true);
			}
			else
			{
				json.put("bracoAtual", -1);
				json.put("bracoObjetivo", -1);
				json.put("braco", false);
			}
			
			if(cintura)
			{
				json.put("cinturaAtual", cinturaAtual);
				json.put("cinturaObjetivo", cinturaObjetivo);
				json.put("cintura", true);
			}
			else
			{
				json.put("cinturaAtual", -1);
				json.put("cinturaObjetivo", -1);
				json.put("cintura", false);
			}
			
			if(costas)
			{
				json.put("costasAtual", costasAtual);
				json.put("costasObjetivo", costasObjetivo);
				json.put("costas", true);
			}
			else
			{
				json.put("costasAtual", -1);
				json.put("costasObjetivo", -1);
				json.put("costas", false);
			}
			
			if(coxas)
			{
				json.put("coxasAtual", coxasAtual);
				json.put("coxasObjetivo", coxasObjetivo);
				json.put("coxas", true);
			}
			else
			{
				json.put("coxasAtual", -1);
				json.put("coxasObjetivo", -1);
				json.put("coxas", false);
			}
			
			if(panturrilha)
			{
				json.put("panturrilhaAtual", panturrilhaAtual);
				json.put("panturrilhaObjetivo", panturrilhaObjetivo);
				json.put("panturrilha", true);
			}
			else
			{
				json.put("panturrilhaAtual", -1);
				json.put("panturrilhaObjetivo", -1);
				json.put("panturrilha", false);
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
			
			Log.e("MinhaTag", "Erro ao transformar o usuario em json");
		}
		
		return json;
	}
	
	public void recuperaUsuarioJSON(JSONObject json)
	{
		try 
		{
			id = json.getInt("id");
			nome = json.getString("nome");
			sobrenome = json.getString("sobrenome");
			pathImgPerfil = json.getString("imagem");
			email = json.getString("email");
			
			pontos = (float)json.getDouble("pontos");
			bonus = (float)json.getDouble("bonus");
			
			dia = json.getString("dia");
			mes = json.getString("mes");
			ano = json.getString("ano");
			
			treinos.clear();
			
			for(int i = 0; i < json.getJSONArray("treinos").length(); i++)
			{
				Treino t = new Treino();
				
				t.recuperaTreinoJSON(json.getJSONArray("treinos").getJSONArray(i));
				
				treinos.add(t);
			}
			
			dietas.clear();
			
			for(int i = 0; i < json.getJSONArray("dietas").length(); i++)
			{
				Dieta d = new Dieta();
				//Log.i("MinhaTag", "Usuario tentando recuperar dieta: " + json.getJSONArray("dietas").toString());
				d.recuperaDietaJSON(json.getJSONArray("dietas").getJSONArray(i));
				
				//d.DebugaDados();
				dietas.add(d);
			}
			
			grupos.clear();
			
			for(int i = 0; i < json.getJSONArray("grupos").length(); i++)
			{
				Grupo g = new Grupo();
				
				g.recuperaGrupoJSON(json.getJSONArray("grupos").getJSONObject(i));
				
				grupos.add(g);
			}
			
			pesoAtual = (float) json.getDouble("pesoAtual");
			pesoObjetivo = (float) json.getDouble("pesoObjetivo");
			peso = json.getBoolean("peso");
			
			toraxAtual = (float) json.getDouble("toraxAtual");
			toraxObjetivo = (float) json.getDouble("toraxObjetivo");
			torax = json.getBoolean("torax");
			
			bracoAtual = (float) json.getDouble("bracoAtual");
			bracoObjetivo = (float) json.getDouble("bracoObjetivo");
			braco = json.getBoolean("braco");
			
			cinturaAtual = (float) json.getDouble("cinturaAtual");
			cinturaObjetivo = (float) json.getDouble("cinturaObjetivo");
			cintura = json.getBoolean("cintura");
			
			costasAtual = (float) json.getDouble("costasAtual");
			costasObjetivo = (float) json.getDouble("costasObjetivo");
			costas = json.getBoolean("costas");
			
			coxasAtual = (float) json.getDouble("coxasAtual");
			coxasObjetivo = (float) json.getDouble("coxasObjetivo");
			coxas = json.getBoolean("coxas");
			
			panturrilhaAtual = (float) json.getDouble("panturrilhaAtual");
			panturrilhaObjetivo = (float) json.getDouble("panturrilhaObjetivo");
			panturrilha = json.getBoolean("panturrilha");
			
		} catch (JSONException e) {
			
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao recuperar o usuario de um JSON");
		}
	}
}
