package modelos;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Grupo 
{
	private int id;
	private String nome;
	private String descricao;
	private String adm;
	
	ArrayList<UsuarioGrupo> usuariosDoGrupo = new ArrayList<UsuarioGrupo>();
	
	public Grupo()
	{
		usuariosDoGrupo.clear();
	}
	
	public Grupo(Usuario u)
	{
		UsuarioGrupo uG = new UsuarioGrupo();
		
		uG.setNomeUsuarioGrupo(u.getNome());
		uG.setPontosUsuarioGrupo(u.getPontos());
		
		usuariosDoGrupo.add(uG);
	}
	
	public Grupo(String n, String d, String a)
	{
		nome = n;
		descricao = d;
		adm = a;
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int i)
	{
		id = i;
	}
	
	public String getNomeDoGrupo()
	{
		return nome;
	}
	
	public void setNomeDoGrupo(String n)
	{
		nome = n;
	}
	
	public String getDescDoGrupo()
	{
		return descricao;
	}
	
	public void setDescDoGrupo(String n)
	{
		descricao = n;
	}
	
	public String getAdmDoGrupo()
	{
		return adm;
	}
	
	public void setAdmDoGrupo(String n)
	{
		adm = n;
	}
	
	public ArrayList<UsuarioGrupo> getListaDeUsuarios()
	{
		return usuariosDoGrupo;
	}
	
	public void setListaDeUsuarios(ArrayList<UsuarioGrupo> u)
	{
		usuariosDoGrupo = u;
	}
	
	public JSONObject toJSON()
	{
		JSONObject json = new JSONObject();
		
		try 
		{
			json.put("idGrupo", id);
			json.put("nomeGrupo", nome);
			json.put("descGrupo", descricao);
			json.put("admGrupo", adm);
			
			JSONArray jUsuarios = new JSONArray();
			
			for(int i = 0; i < usuariosDoGrupo.size(); i++)
			{
				jUsuarios.put(usuariosDoGrupo.get(i).toJSON());
			}
			
			json.put("usuarios", jUsuarios);
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao tentar transformar um UsuarioGrupo em JSON");
		}
		
		return json;
	}
	
	public void recuperaGrupoJSON(JSONObject json)
	{
		try 
		{			
			id = json.getInt("idGrupo");
			nome = json.getString("nomeGrupo");
			descricao = json.getString("descGrupo");
			adm = json.getString("admGrupo");
			
			JSONArray jUsuarios = json.getJSONArray("usuarios");
			
			usuariosDoGrupo.clear();
			
			for(int i = 0; i < jUsuarios.length(); i++)
			{
				UsuarioGrupo uG = new UsuarioGrupo();
				
				uG.recuperaUsuarioGrupoJSON(jUsuarios.getJSONObject(i));
				
				usuariosDoGrupo.add(uG);
			}
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao tentar recuperar um UsuarioGrupo de um JSON");
		}
	}
}
