package modelos;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class ItemExercicio implements Parcelable
{
	String tipo;
	String exercicio;
	
	int series;
	int repeticoes;
	int descanso;
	
	long id;
	
	public ItemExercicio()
	{
		tipo = "aerobico";
		exercicio = "Supino";
		
		series = 3;
		repeticoes = 10;
		descanso = 60;
		
		id = 1;
	}
	
	public ItemExercicio(Parcel in)
	{		
		tipo = in.readString();
		exercicio = in.readString();
		series = in.readInt();
		repeticoes = in.readInt();
		descanso = in.readInt();
		id = in.readLong();
	}
	
	public long getId()
	{
		return id;
	}
	
	public void setId(long i)
	{
		id = i;
	}
	
	public String getTipo()
	{
		return tipo;
	}
	
	public void setTipo(String s)
	{
		tipo = s;
	}
	
	public String getExercicio()
	{
		return exercicio;
	}
	
	public void setExercicio(String s)
	{
		exercicio = s;
	}
	
	public int getSeries()
	{
		return series;
	}
	
	public void setSeries(int i)
	{
		series = i;
	}
	
	public int getRepeticoes()
	{
		return repeticoes;
	}
	
	public void setRepeticoes(int i)
	{
		repeticoes = i;
	}
	
	public int getDescanso()
	{
		return descanso;
	}
	
	public void setDescanso(int i)
	{
		descanso = i;
	}
	
	public JSONObject toJSON()
	{
		JSONObject json = new JSONObject();
		
		try 
		{
			json.put("tipo", tipo);
			json.put("exercicio", exercicio);
			json.put("series", series);
			json.put("repeticoes", repeticoes);
			json.put("descanso", descanso);
			
			return json;
			
		} catch (JSONException e) {
			e.printStackTrace();
			
			Log.e("MinhaTag", "Erro ao passar um ItemExercicio para JSON");
		}
		
		return null;
	}
	
	public void recuperaItemExercicioJSON(JSONObject j)
	{
		try 
		{
			tipo = j.getString("tipo");
			exercicio = j.getString("exercicio");
			series = j.getInt("series");
			repeticoes = j.getInt("repeticoes");
			descanso = j.getInt("descanso");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("MinhaTag", "Erro ao recuperar um ItemExercicio de um JSON");
		}
		
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		
		out.writeString(tipo);
		out.writeString(exercicio);
		out.writeInt(series);
		out.writeInt(repeticoes);
		out.writeInt(descanso);
		out.writeLong(id);
	}
	
	 public static final Parcelable.Creator<ItemExercicio> CREATOR = new Parcelable.Creator<ItemExercicio>() {
		 public ItemExercicio createFromParcel(Parcel in) 
		 {
		   	return new ItemExercicio(in);
		 }

		 public ItemExercicio[] newArray(int size) 
		 {
		   	return new ItemExercicio[size];
		 }
	 };

}
